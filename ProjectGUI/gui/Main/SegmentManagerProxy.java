package gui.Main;

public class SegmentManagerProxy
{
    private GlobalObjectId globalObjectId;

    public SegmentManagerProxy(GlobalObjectId globalObjectId){
        this.globalObjectId = globalObjectId;
    }

    public void register(GlobalObjectId p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "gui.Main.GlobalObjectId";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "register", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void exitGUI(GlobalObjectId p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "gui.Main.GlobalObjectId";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "exitGUI", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void createMaster(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "createMaster", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void createSlave(String ip, int port, int key){
        String[] parameterTypeNames = new String[3];
        parameterTypeNames[0] = "java.lang.String";
        parameterTypeNames[1] = "int";
        parameterTypeNames[2] = "int";
        Object[] actualParameters = new Object[3];
        actualParameters[0] = ip;
        actualParameters[1] = port;
        actualParameters[2] = key;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "createSlave", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void shutdownSegment(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "shutdownSegment", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void saveHyPeerWeb(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "saveHyPeerWeb", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void addNode(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "addNode", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void shutdownHyPeerWeb(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "shutdownHyPeerWeb", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void removeNode(int p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "int";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "removeNode", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void broadcast(int nodeID, String message){
        String[] parameterTypeNames = new String[2];
        parameterTypeNames[0] = "int";
        parameterTypeNames[1] = "java.lang.String";
        Object[] actualParameters = new Object[2];
        actualParameters[0] = nodeID;
        actualParameters[1] = message;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "broadcast", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    
    public void sendMessage(int nodeID, Integer targetID, String message){
        String[] parameterTypeNames = new String[3];
        parameterTypeNames[0] = "int";
        parameterTypeNames[1] = "int";
        parameterTypeNames[2] = "java.lang.String";
        Object[] actualParameters = new Object[3];
        actualParameters[0] = nodeID;
        actualParameters[1] = targetID;
        actualParameters[2] = message;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "sendMessage", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public boolean equals(java.lang.Object p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "java.lang.Object";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "equals", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Boolean)result;
    }

    public java.lang.String toString(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "toString", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (java.lang.String)result;
    }

    public int hashCode(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "hashCode", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Integer)result;
    }

	public void saveToDBButton() 
	{
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "saveToDBButton", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
	}
	
	public void loadFromDBButton() 
	{
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.SegmentManager", "loadFromDBButton", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
	}

}
