package gui.Main;

import gui.Main.HyPeerWebDebugger;
import gui.newWindows.JoinHypeerwebWindow;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.*;

/**
 * The central GUI used to display information about the HyPeerWeb and debug information
 * 
 * @author Matthew Smith
 *
 */
public class GUI extends JFrame
{
	private static GUI singleton = null;
	private static GlobalObjectId myId;
	
	/** Main Debugger Panel**/
	private HyPeerWebDebugger debugger;
	
	private SegmentManagerProxy hypeerwebsegment;
	private JScrollPane scrollPane;
	private JoinHypeerwebWindow joinWindow = null;
	
	/**
	 * Creates and initializes the GUI as being the root
	 */
	public GUI(){
		
//		this.hypeerweb = hypeerweb;
//		hypeerweb.addNode(new Node(0));
		this.setTitle("HyPeerWeb DEBUGGER");

		this.addWindowListener(new WindowAdapter() {
			  public void windowClosing(WindowEvent we) {
				close();
				shutdown();
				System.exit(0);
			  }
			});
		
		debugger = new HyPeerWebDebugger(this);
		scrollPane = new JScrollPane(debugger);
		scrollPane.setPreferredSize(new Dimension(debugger.WIDTH+20, debugger.HEIGHT));
		
		this.getContentPane().add(scrollPane);
		this.pack();
		joinWindow = new JoinHypeerwebWindow(this, "Starting up...");
	}
	
	public GlobalObjectId getMyId()
	{
		return myId;
	}
	
	public void shutdown(){
		hypeerwebsegment.exitGUI(myId);
	}
	
	public static GUI getSingleton(){
		if(singleton == null){
			try{
				singleton = new GUI();
						
				singleton.setVisible(true);
			}
			catch(Exception e)	{
				JOptionPane.showMessageDialog(null, e.getMessage(), "ERROR",JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
				singleton.hypeerwebsegment.exitGUI(myId);
				System.exit(1);
			}
		}
		return singleton;
	}
	
	/**
	 * Start Point of the Program
	 */
	public static void main (String[] args){
		EventQueue.invokeLater(new Runnable() {
			public void run() 
			{
				try
				{
					GUI.getSingleton();
				}
				catch(Exception e) 
				{
					e.printStackTrace();	
				}
			}
		});
		
		
	}
	
	public SegmentManagerProxy getHyPeerWebSegment()
	{
		return hypeerwebsegment;
	}

	/**
	 * Retrieves the HyPeerWeb Debugging Panel
	 * @return HyPeerWebDebugger
	 */
	public HyPeerWebDebugger getHyPeerWebDebugger() {
		return debugger;
	}

	
	/**
	 * This allows Hypeerweb to Print to the GUI.  Just pass in an object... probably a string but can be html.
	 * @param msg
	 * @phase 6
	 */
	
	public void finalize(){
		hypeerwebsegment.exitGUI(myId);
	}

	public void connect(String ip, int portInt, int key) 
	{
		System.out.println("GUI.connect()");
		
		PortNumber myPort = new PortNumber(portInt+10);
//		PortNumber.setApplicationsPortNumber(myPort);
		LocalObjectId myLocalId = new LocalObjectId();

		PeerCommunicator.createPeerCommunicator(myPort);
		GlobalObjectId serverGlobalObjectId = new GlobalObjectId(ip,new PortNumber(portInt),new LocalObjectId(key));
		
		hypeerwebsegment = new SegmentManagerProxy(serverGlobalObjectId);
		
		System.out.println("About to register...");
		

		
		myId = new GlobalObjectId(myLocalId);
		ObjectDB.getSingleton().store(myLocalId, this);
		
		
		System.out.println("myId:"+myId);
		
		hypeerwebsegment.register(myId);
		
		System.out.println("Done");		
	}
	
	public void draw(ArrayList<Integer> webIDs)
	{
		debugger.getMapper().getNodeListing().setNewList(webIDs);
	}
	
	public void kill()
	{
		this.dispose();
		
		//Todo: What should kill do? -BP
		/*
		hypeerwebsegment.deleteAllNodes();
		*/
		hypeerwebsegment.exitGUI(getMyId());
		close();
		System.exit(0);
	}
	
	public void printToError(Object msg)
	{
		debugger.getStatus().setContent(msg.toString());
	}
	public void printToTracePanel(Object msg){
		debugger.getTracePanel().print(msg);
	}

	public void close()
	{
		PeerCommunicator.stopThisConnection();
		hypeerwebsegment.exitGUI(myId);
	}
}
