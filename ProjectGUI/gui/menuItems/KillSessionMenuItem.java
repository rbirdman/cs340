package gui.menuItems;

import gui.Main.GUI;
import gui.Main.PeerCommunicator;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JMenuItem;

public class KillSessionMenuItem extends JMenuItem implements ActionListener
{
	public GUI main;
	
	public KillSessionMenuItem (GUI main)
	{
		this.main = main;
		
		init();
	}
	
	/**
	 * initializes the GUI components
	 */
	public void init()
	{
		this.setText("Kill Entire HypeerWeb");	
		this.addActionListener(this);
	}
	
	/**
	 * Action when menu item is pressed
	 */
	public void actionPerformed(ActionEvent e) 
	{
		

		main.getHyPeerWebSegment().shutdownHyPeerWeb();
		main.close();
		
		main.dispose();
		System.exit(0);
	}
}