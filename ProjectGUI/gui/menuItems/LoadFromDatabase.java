package gui.menuItems;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import gui.Main.GUI;

public class LoadFromDatabase extends JMenuItem implements ActionListener
{
	GUI main;
	
	public LoadFromDatabase(GUI main)
	{
		this.main = main;
		
		init();
	}
	
	public void init()
	{
		this.setText("Load HypeerWeb from Database");
		
		this.addActionListener(this);
	}
	
	/**
	 * Action when menu item is pressed
	 */
	public void actionPerformed(ActionEvent e) 
	{
		main.getHyPeerWebSegment().loadFromDBButton();
		// load from database
		//main.getHyPeerWebDebugger().getStandardCommands().LoadButtonPressed();
		
	}
}
