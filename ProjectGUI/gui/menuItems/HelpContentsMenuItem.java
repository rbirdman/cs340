package gui.menuItems;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.Enumeration;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import gui.Main.GUI;
import gui.Main.ObjectDB;

/**
 * Represents the Help menu item presented in the Help menu
 * 
 * @author Matthew Smith
 *
 */
public class HelpContentsMenuItem extends JMenuItem implements ActionListener{

	GUI main;
	
	/**
	 * Creates a Help menu Item
	 * @param main
	 */
	public HelpContentsMenuItem (GUI main)
	{
		this.main = main;
		
		init();
	}
	
	/**
	 * initializes the GUI components
	 */
	public void init()
	{
		this.setText("Contents");
		
		this.addActionListener(this);
	}
	
	/**
	 * Action when menu item is pressed
	 */
	public void actionPerformed(ActionEvent e) {
		
		Enumeration<Object> list= ObjectDB.getSingleton().enumeration();
		java.util.ArrayList<String> nodes = new java.util.ArrayList<String>();
		// ALL YOUR BASE ARE BELONG TO US!
		while(list.hasMoreElements())
		{
			Object o=list.nextElement();
			//if(o instanceof Node && !(o instanceof NodeProxy))
			//{
				//Node n=(Node)o;
				nodes.add(o.toString());
		}

		
		System.out.println(nodes.toString());
	}

}
