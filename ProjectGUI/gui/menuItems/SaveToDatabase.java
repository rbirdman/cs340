package gui.menuItems;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JMenuItem;
import gui.Main.GUI;


public class SaveToDatabase extends JMenuItem implements ActionListener
{
	GUI main;
	
	public SaveToDatabase(GUI main)
	{
		this.main = main;
		
		init();
	}
	
	public void init()
	{
		this.setText("Save HypeerWeb To Database");
		
		this.addActionListener(this);
	}
	
	/**
	 * Action when menu item is pressed
	 */
	public void actionPerformed(ActionEvent e) 
	{
		main.getHyPeerWebSegment().saveToDBButton();
		//save stuff
		//main.getHyPeerWebDebugger().getStandardCommands().saveButtonPressed();
		
	}
}
