package gui.newWindows;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import gui.Main.GUI;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.Point;

public class NewSegmentPanel extends JPanel 
{	
	protected GUI main;
	protected JLabel IPAddressLabel;
	protected JLabel PortLabel;
	protected JLabel MasterKeyLabel;
    protected JTextField IPAddress;
    protected JTextField Port;
    protected JTextField Key;
    protected JButton ConnectButton;

	/**
	 * Create the panel.
	 */
	public NewSegmentPanel(GUI main) 
	{
		setLocation(new Point(120, 120));
		setMinimumSize(new Dimension(400, 400));
		setSize(new Dimension(400, 400));
    	this.main = main;
        
		ConnectButton = new JButton("Connect to Segment");
		ConnectButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				ConnectPressed();				
			} 
			public void windowClosing(WindowEvent we) 
			{
				setNewHypeerwebSegmentWindowToNull();  
			} 
		});
		
		JPanel SegmentPanel = new JPanel();
		SegmentPanel.setMinimumSize(new Dimension(400, 400));
		SegmentPanel.setLocation(new Point(120, 120));
		GridBagLayout gbl_SegmentPanel = new GridBagLayout();
		gbl_SegmentPanel.columnWidths = new int[]{207, 86};
		gbl_SegmentPanel.rowHeights = new int[] {20, 20, 20, 20};
		gbl_SegmentPanel.columnWeights = new double[]{0.0, 0.0};
		gbl_SegmentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		SegmentPanel.setLayout(gbl_SegmentPanel);
        
		GridBagConstraints gbc_ConnectButton = new GridBagConstraints();
		gbc_ConnectButton.gridwidth = 2;
		gbc_ConnectButton.gridy = 3;
		gbc_ConnectButton.gridx = 0;
		SegmentPanel.add(ConnectButton, gbc_ConnectButton);
		this.add(SegmentPanel);
	}
	
	private void setNewHypeerwebSegmentWindowToNull()
	{
	  	main.getHyPeerWebDebugger().getStandardCommands().setNewHypeerwebSegmentWindowToNull();
	}
	
	private void ConnectPressed()
	{
		main.getHyPeerWebSegment().createMaster();
		
		SwingUtilities.getWindowAncestor(this).setVisible(false);
	}
}