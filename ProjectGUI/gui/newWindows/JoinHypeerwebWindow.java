package gui.newWindows;

import java.awt.Dimension;
import java.awt.Point;

import gui.Main.GUI;

@SuppressWarnings("serial")
public class JoinHypeerwebWindow extends PopUpWindow
{
	public JoinHypeerwebWindow(GUI main, String title)
	{
		super(main, title);
	}
		
	@Override
	protected void addPanel()
	{
		panel = new JoinHypeerwebSegmentPanel(main);
		this.add(panel);
		this.setAlwaysOnTop(true);
		setMinimumSize(new Dimension(400, 150));
		this.setLocation(new Point(120, 120));
		this.pack();
	}
}