package gui.newWindows;
import java.awt.Dimension;
import java.awt.Point;

import gui.Main.GUI;


@SuppressWarnings("serial")
public class NewSegmentWindow extends PopUpWindow
{

	public NewSegmentWindow(GUI main, String title)
	{
		super(main, title);
	}
	
	@Override
	protected void addPanel()
	{
		panel = new NewSegmentPanel(main);
		this.add(panel);
		setLocation(new Point(120, 120));
		setMinimumSize(new Dimension(400, 150));
		pack();
	}

}