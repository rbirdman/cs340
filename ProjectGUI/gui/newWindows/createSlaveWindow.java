package gui.newWindows;

import java.awt.Dimension;
import java.awt.Point;

import gui.Main.GUI;

@SuppressWarnings("serial")
public class createSlaveWindow extends PopUpWindow 
{

	public createSlaveWindow(GUI main, String title)
	{
		super(main, title);
	}
	
	@Override
	protected void addPanel()
	{
		panel = new slavePanel(main);
		this.add(panel);
		this.setAlwaysOnTop(true);
		setMinimumSize(new Dimension(400, 150));
		this.setLocation(new Point(120, 120));
		this.pack();
	}
}