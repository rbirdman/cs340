package gui.newWindows;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import gui.Main.GUI;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Dimension;
import java.awt.Point;

@SuppressWarnings("serial")
public class JoinHypeerwebSegmentPanel extends JPanel 
{
	protected GUI main;
	protected JLabel IPAddressLabel;
	protected JLabel PortLabel;
	protected JLabel MasterKeyLabel;
    protected JTextField IPAddress;
    protected JTextField Port;
    protected JTextField Key;
    protected JButton ConnectButton;

	/**
	 * Create the panel join panel
	 */
	public JoinHypeerwebSegmentPanel(GUI main) 
	{
		setLocation(new Point(120, 120));
		setMinimumSize(new Dimension(400, 400));
		setSize(new Dimension(400, 400));
		this.setPreferredSize(new Dimension(400, 400));
    	this.main = main;
        
		ConnectButton = new JButton("Connect to Segment");
		ConnectButton.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e) 
			{
				ConnectPressed();				
			} 
			public void windowClosing(WindowEvent we) 
			{
				setJoinHypeerwebSegmentWindowToNull();  
			} 
		});
		
		JPanel SegmentPanel = new JPanel();
		SegmentPanel.setMinimumSize(new Dimension(400, 400));
		SegmentPanel.setLocation(new Point(120, 120));
		GridBagLayout gbl_SegmentPanel = new GridBagLayout();
		gbl_SegmentPanel.columnWidths = new int[]{207, 86};
		gbl_SegmentPanel.rowHeights = new int[] {20, 20, 20, 20};
		gbl_SegmentPanel.columnWeights = new double[]{0.0, 0.0};
		gbl_SegmentPanel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		SegmentPanel.setLayout(gbl_SegmentPanel);
     
        IPAddressLabel = new JLabel("IP Address");
        GridBagConstraints gbc_IPAddressLabel = new GridBagConstraints();
        gbc_IPAddressLabel.insets = new Insets(0, 0, 5, 5);
        gbc_IPAddressLabel.gridx = 0;
        gbc_IPAddressLabel.gridy = 0;
        SegmentPanel.add(IPAddressLabel, gbc_IPAddressLabel);
		
        IPAddress= new JTextField(15);
        IPAddress.setText("127.0.0.1");
        
        GridBagConstraints gbc_IPAddress = new GridBagConstraints();
        gbc_IPAddress.anchor = GridBagConstraints.NORTHWEST;
        gbc_IPAddress.insets = new Insets(0, 0, 5, 0);
        gbc_IPAddress.gridx = 1;
        gbc_IPAddress.gridy = 0;
        SegmentPanel.add(IPAddress, gbc_IPAddress);
		PortLabel = new JLabel("Port");
		GridBagConstraints gbc_PortLabel = new GridBagConstraints();
		gbc_PortLabel.insets = new Insets(0, 0, 5, 5);
		gbc_PortLabel.gridx = 0;
		gbc_PortLabel.gridy = 1;
		SegmentPanel.add(PortLabel, gbc_PortLabel);
		Port = new JTextField(15);
		Port.setText("49200");
		GridBagConstraints gbc_Port = new GridBagConstraints();
		gbc_Port.anchor = GridBagConstraints.NORTHWEST;
		gbc_Port.insets = new Insets(0, 0, 5, 0);
		gbc_Port.gridx = 1;
		gbc_Port.gridy = 1;
		SegmentPanel.add(Port, gbc_Port);
		MasterKeyLabel = new JLabel("Key");
		GridBagConstraints gbc_MasterKeyLabel = new GridBagConstraints();
		gbc_MasterKeyLabel.insets = new Insets(0, 0, 5, 5);
		gbc_MasterKeyLabel.gridx = 0;
		gbc_MasterKeyLabel.gridy = 2;
		SegmentPanel.add(MasterKeyLabel, gbc_MasterKeyLabel);
		Key = new JTextField(15);
		Key.setText("-2147483648");
		GridBagConstraints gbc_Key = new GridBagConstraints();
		gbc_Key.anchor = GridBagConstraints.NORTHEAST;
		gbc_Key.insets = new Insets(0, 0, 5, 0);
		gbc_Key.gridx = 1;
		gbc_Key.gridy = 2;
		SegmentPanel.add(Key, gbc_Key);
		
		GridBagConstraints gbc_ConnectButton = new GridBagConstraints();
		gbc_ConnectButton.gridwidth = 2;
		gbc_ConnectButton.gridy = 3;
		gbc_ConnectButton.gridx = 0;
		SegmentPanel.add(ConnectButton, gbc_ConnectButton);
		this.add(SegmentPanel);
		this.requestFocus();
		IPAddress.requestFocus();
		main.pack();
    }
    
    private void setJoinHypeerwebSegmentWindowToNull()
    {
    	main.getHyPeerWebDebugger().getStandardCommands().setJoinHypeerwebSegmentWindowToNull();
    }
    
    /**
	 * @designer Tim Atwood
	 * @phase 6
	 * Connects us to the segment.  Called by the connectButton listener
	 */
    private void ConnectPressed()
    {
    	try {
    		String ip = IPAddress.getText();
        	int port = Integer.parseInt(Port.getText());
        	int key =  Integer.parseInt(Key.getText());
//    		main.getHyPeerWebSegment().createSlave(ip,port,key);
        	System.out.println("What?");
        	// Funciton call pass in main
        	// to do
        	GUI.getSingleton().connect(ip,port,key);    	
        	SwingUtilities.getWindowAncestor(this).setVisible(false);
    	}
    	catch(NumberFormatException e) {
    		JOptionPane.showMessageDialog(this, e.getMessage());
    	}
    	
    	
    }
}
