package edu.byu.cs340.aabp.hypeerweb;

import edu.byu.cs340.aabp.hypeerweb.phase6.ObjectDB;
import gui.Main.GlobalObjectId;
import gui.Main.LocalObjectId;
import gui.Main.PortNumber;

public class HyPeerWebSlave extends HyPeerWeb 
{
	private static String masterIP;
	private static int masterPort;
	private static int masterKey;
	
	private static HyPeerWebMasterProxy master;
	
	private static GlobalObjectId myId;

	
	private HyPeerWebSlave()
	{
		super();
		LocalObjectId myLocalId = new LocalObjectId();
		ObjectDB.getSingleton().store(myLocalId, this);
		
		System.out.println("HyPeerWebSlave()");
				
		GlobalObjectId masterGlobalObjectId = new GlobalObjectId(masterIP,new PortNumber(masterPort),new LocalObjectId(masterKey));
		master = new HyPeerWebMasterProxy(masterGlobalObjectId);
		
		System.out.println("About to register...");
		
		myId = new GlobalObjectId(myLocalId);		
		
		System.out.println("myId:"+myId);
		
		master.register(myId);
		
		System.out.println("Done");		

	}
	
	public static void setIP(String newIP)
	{
		masterIP = newIP;
	}
	public static void setPort(int newPort)
	{
		masterPort = newPort;
	}
	public static void setKey(int newKey)
	{
		masterKey = newKey;
	}
	
	static public HyPeerWeb getSingleton()
	{
		if(hpw == null)
		{
			hpw = new HyPeerWebSlave();
			return hpw;
		}
		return hpw;
	}

	@Override
	public Node getLastNode() 
	{
		if(lastNode != null)
		{
			return lastNode;
		}
		
		return master.getLastNode();
	}
	
	public Node getLocalLastNode() 
	{
			return lastNode;
	}

	@Override
	public void refreshEverything() 
	{
		SegmentManager.getSingleton().draw();
	}

	@Override
	public void saveToDBButton() 
	{
		master.saveToDBButton();
	}

	@Override
	public void loadFromDBButton() 
	{
		master.loadFromDBButton();
	}
	
	@Override
	public void saveToDatabase() 
	{
		HyPeerWebDatabase.initHyPeerWebDatabase();
		saveToDatabaseWorker();
		HyPeerWebDatabase.destroyHyPeerWebDatabase();
	}
}
