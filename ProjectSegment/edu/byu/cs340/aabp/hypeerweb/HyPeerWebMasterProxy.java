package edu.byu.cs340.aabp.hypeerweb;

import edu.byu.cs340.aabp.hypeerweb.phase6.PeerCommunicator;
import gui.Main.Command;
import gui.Main.GlobalObjectId;

public class HyPeerWebMasterProxy
{
    private GlobalObjectId globalObjectId;

    public HyPeerWebMasterProxy(GlobalObjectId globalObjectId){
        this.globalObjectId = globalObjectId;
    }

    public void register(gui.Main.GlobalObjectId p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "gui.Main.GlobalObjectId";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWebMaster", "register", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void clear(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "clear", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public boolean contains(edu.byu.cs340.aabp.hypeerweb.Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "contains", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Boolean)result;
    }

    public int size(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "size", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Integer)result;
    }

    public void close(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "close", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public edu.byu.cs340.aabp.hypeerweb.HyPeerWebDatabase getHyPeerWebDatabase(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "getHyPeerWebDatabase", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (edu.byu.cs340.aabp.hypeerweb.HyPeerWebDatabase)result;
    }

    public void setLastNode(edu.byu.cs340.aabp.hypeerweb.Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "setLastNode", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void saveToDatabase(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "saveToDatabase", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void addNode(edu.byu.cs340.aabp.hypeerweb.Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "addNode", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void addToHyPeerWeb(edu.byu.cs340.aabp.hypeerweb.Node p0, edu.byu.cs340.aabp.hypeerweb.Node p1){
        String[] parameterTypeNames = new String[2];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.Node";
        parameterTypeNames[1] = "edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[2];
        actualParameters[0] = p0;
        actualParameters[1] = p1;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "addToHyPeerWeb", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public edu.byu.cs340.aabp.hypeerweb.Node getNode(int p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "int";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "getNode", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (edu.byu.cs340.aabp.hypeerweb.Node)result;
    }

    public void reload(java.lang.String p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "java.lang.String";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "reload", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void reload(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "reload", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void removeNode(edu.byu.cs340.aabp.hypeerweb.Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWeb", "removeNode", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public boolean equals(java.lang.Object p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "java.lang.Object";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "equals", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Boolean)result;
    }

    public java.lang.String toString(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "toString", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (java.lang.String)result;
    }

    public int hashCode(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "hashCode", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Integer)result;
    }

	public edu.byu.cs340.aabp.hypeerweb.Node getLastNode() 
	{
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWebMaster", "getLastNode", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (edu.byu.cs340.aabp.hypeerweb.Node)result;
	}

	public void saveToDBButton() {
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWebMaster", "saveToDBButton", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);		
	}

	public void loadFromDBButton() {
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.HyPeerWebMaster", "loadFromDBButton", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);		
	}

}
