package edu.byu.cs340.aabp.hypeerweb;

import java.util.ArrayList;

import edu.byu.cs340.aabp.hypeerweb.phase6.ObjectDB;
import gui.Main.GlobalObjectId;
import gui.Main.LocalObjectId;

public class HyPeerWebMaster extends HyPeerWeb 
{
	public ArrayList<HyPeerWebSlaveProxy> slaves;

	private HyPeerWebMaster()
	{
		super();
		LocalObjectId managerID = new LocalObjectId();
		ObjectDB.getSingleton().store(managerID, this);
		System.out.println("Master Segment Key:\t"+managerID+"\t(Use this to connect the slave segments to the master 	segment)");
		slaves = new ArrayList<HyPeerWebSlaveProxy>();
	}
	
	static public HyPeerWeb getSingleton()
	{
		if(hpw == null)
		{
			hpw = new HyPeerWebMaster();
			return hpw;
		}
		return hpw;
	}
	
	public void register(GlobalObjectId globalID)
	{
		try
		{
			System.out.println("HyPeerWebMaster.register()");
			slaves.add(new HyPeerWebSlaveProxy(globalID));
			System.out.println("New Connection:"+globalID);
			
			System.out.println("HyPeerWebMaster.register() done");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("slaves.size(): "+slaves.size());
	}

	@Override
	public Node getLastNode() 
	{
		if(lastNode != null)
		{
			return lastNode;
		}
		for(HyPeerWebSlaveProxy p : slaves)
		{
			Node n = p.getLocalLastNode();
			if(n != null)
			{
				return n;
			}
		}
		return null;
	}
	
	@Override
	public void refreshEverything()
	{
		for(HyPeerWebSlaveProxy p : slaves)
		{
			p.refreshEverything();
		}
	}

	@Override
	public void saveToDBButton() 
	{
		saveToDatabase();
		for(HyPeerWebSlaveProxy p : slaves)
		{
			p.saveToDatabase();
		}
	}

	@Override
	public void loadFromDBButton() 
	{
		reload();
		for(HyPeerWebSlaveProxy p : slaves)
		{
			p.reload();
		}

	}

	@Override
	public void saveToDatabase() 
	{
		HyPeerWebDatabase.initHyPeerWebDatabase();
		hdb.deleteEverythingFromDatabase();
		saveToDatabaseWorker();
		HyPeerWebDatabase.destroyHyPeerWebDatabase();
	}
	

}
