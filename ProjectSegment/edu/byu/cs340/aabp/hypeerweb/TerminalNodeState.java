/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.byu.cs340.aabp.hypeerweb;

/**
 *
 * @author rbirdman
 */
public class TerminalNodeState extends StandardNodeState 
{
	public static final int STATE_ID = 4;
	
	public static final TerminalNodeState STATE_CLASS = new TerminalNodeState();
			
	private TerminalNodeState() 
	{
		
	}
	
	@Override
	public String toString() 
	{
		return "Terminal Node State";
	}
}
