/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.byu.cs340.aabp.hypeerweb;

/**
 *
 * @author rbirdman
 */
public class DownPointingNodeState extends StandardNodeState 
{
	public static final int STATE_ID = 2;
	
	public static final DownPointingNodeState STATE_CLASS = new DownPointingNodeState();
			
	private DownPointingNodeState() 
	{
		
	}
	
	@Override
	public String toString() 
	{
		return "Down Pointing Node State";
	}
}
