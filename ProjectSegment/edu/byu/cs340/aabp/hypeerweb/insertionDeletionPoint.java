package edu.byu.cs340.aabp.hypeerweb;

/**
 * Pair class to hold both insertion and deletion points
 * @author Tim
 *
 */
public class insertionDeletionPoint 
{
    private Node deletionPoint; //first member of pair
    private Node insertionPoint; //second member of pair

    public insertionDeletionPoint()
    {
    	deletionPoint = Node.NULL_NODE;
    	insertionPoint = Node.NULL_NODE;
    }
    
    public insertionDeletionPoint(Node dPoint, Node iPoint) 
    {
        setDeletionPoint(dPoint);
        setInsertionPoint(iPoint);
    }

	public Node getDeletionPoint() 
	{
		return deletionPoint;
	}

	public void setDeletionPoint(Node deletionPoint) 
	{
		this.deletionPoint = deletionPoint;
	}

	public Node getInsertionPoint() 
	{
		return insertionPoint;
	}

	public void setInsertionPoint(Node insertionPoint) 
	{
		this.insertionPoint = insertionPoint;
	}
	
	public boolean isCapNode()
	{
		boolean toPass = false;
		int valueOfNode = deletionPoint.getWebId().getValue();
		valueOfNode = valueOfNode ^ 0;
		if(valueOfNode == 1)
		{
			toPass = true;
		}
		return toPass;
	}
}
