package edu.byu.cs340.aabp.hypeerweb;

public class DisconnectEvenNode extends DisconnectTemplate {

	@Override
	protected void subDisconnect(Node thisNode) 
	{
		thisNode.getFold().setFold(Node.NULL_NODE);
		thisNode.getFold().setSurrogateFold(thisNode.getParent());
		thisNode.getParent().setInverseSurrogateFold(thisNode.getFold());
	}

}
