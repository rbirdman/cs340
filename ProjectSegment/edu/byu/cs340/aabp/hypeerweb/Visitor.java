	/**
	 * @phase 4
	 * @author Benjamin Parry
	 * 
	 * The Visitor Interface
	 */

package edu.byu.cs340.aabp.hypeerweb;

public interface Visitor 
{
	void visit (Node node, Parameters parameters);
}