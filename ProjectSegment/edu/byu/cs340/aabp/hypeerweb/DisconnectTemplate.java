package edu.byu.cs340.aabp.hypeerweb;

public abstract class DisconnectTemplate 
{
	
	public void disconnect(Node thisNode)
	{
//		System.out.println("disconnect - "+getWebId().getValue());
		// Height
		thisNode.getParent().getWebId().incrHeight(-1);
		
		subDisconnect(thisNode);
		
		Node parent = thisNode.getParent();
		//Neighbors, Surrogate Neighbors, Inverse Surrogate Neighbors
		for (Node n : thisNode.getNeighbors())
		{
			n.removeNeighbor(thisNode);
			if (!n.equals(parent))
			{
				n.addSurrogateNeighbor(parent);
				parent.addInverseSurrogateNeighbor(n);
			}
		}
		
		for (Node n : thisNode.getSurrogateNeighbors())
		{
			n.removeInverseSurrogateNeighbor(thisNode);
		}
	}
	
	abstract void subDisconnect(Node thisNode); // Protected verifies that this will never be called outside of template algorithm.
	

}
