package edu.byu.cs340.aabp.hypeerweb;

public class DisconnectOddNode extends DisconnectTemplate {

	@Override
	protected void subDisconnect(Node thisNode) 
	{
		thisNode.getFold().setFold(thisNode.getFold().getInverseSurrogateFold());
		thisNode.getFold().getFold().setFold(thisNode.getFold());
		thisNode.getFold().getFold().setSurrogateFold(Node.NULL_NODE);
		thisNode.getFold().setInverseSurrogateFold(Node.NULL_NODE);
	}

}
