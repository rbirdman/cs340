package edu.byu.cs340.aabp.hypeerweb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import edu.byu.cs340.aabp.hypeerweb.phase6.Broadcaster;
import edu.byu.cs340.aabp.hypeerweb.phase6.GUIProxy;
import edu.byu.cs340.aabp.hypeerweb.phase6.GUISender;
import edu.byu.cs340.aabp.hypeerweb.phase6.ObjectDB;
import edu.byu.cs340.aabp.hypeerweb.phase6.PeerCommunicator;

import gui.Main.Command;
import gui.Main.GlobalObjectId;
import gui.Main.LocalObjectId;

public class SegmentManager 
{
	private HyPeerWeb hpw;
	public ArrayList<GUIProxy> guis;
	private static SegmentManager segmentManager=null;
	
	private SegmentManager()
	{
		hpw = null;
		guis = new ArrayList<GUIProxy>();
	}
	static public SegmentManager getSingleton(){
		if(segmentManager==null){
			segmentManager =  new SegmentManager();
		}
		return segmentManager;
	}
	
	
	public void createMaster()
	{
		hpw = HyPeerWebMaster.getSingleton();
	}
	
	public void createSlave(String ip, int port, int key)
	{
		HyPeerWebSlave.setIP(ip);
		HyPeerWebSlave.setPort(port);
		HyPeerWebSlave.setKey(key);
		
		hpw = HyPeerWebSlave.getSingleton();
	}
	
	public void register(GlobalObjectId globalID)
	{
		GUIProxy guiproxy = new GUIProxy(globalID);
		try
		{
			System.out.println("SegmentManager.register()");
			guis.add(guiproxy);
			System.out.println("New Connection:"+globalID);
			
			System.out.println("SegmentManager.register() done");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		System.out.println("guis.size():"+guis.size());
		
		if (hpw != null)
		{
			java.util.ArrayList<Integer> nodes = getCurrentSegmentNodes();
			guiproxy.draw(nodes);
		}
	}
	
	public void exitGUI(GlobalObjectId globalID)
	{
		int i = guis.indexOf(new GUIProxy(globalID));
		if (i >= 0)
			guis.remove(i);
		System.out.println("exitGUI: post: guis.size():"+guis.size());
	}
	
	public void saveHyPeerWeb()
	{
		System.out.println("Segment Manager.save() : )");
		if (hpw != null)
			hpw.saveToDatabase();
	}
	
	public void addNode()
	{
		if (hpw == null)
		{
			printToError("You can't add a node because the segment hasn't been created yet.");
			return;
		}
		
		Node newNode = new Node(1);
		LocalObjectId id = new LocalObjectId();
		newNode.setLocalID(id);
		
		ObjectDB.getSingleton().store(id, newNode);
		hpw.addNode(newNode);
		draw();
	//	System.out.println("hpw.size()" + hpw.size());
	}
	
	
	public void removeNode(int id)
	{
		if (hpw == null)
		{
			printToError("You can't delete a node because the segment hasn't been created yet.");
			return;
		}
		Node node = hpw.getNode(id);
		if (node == null)
		{
			printToError("Node " + id + " doesn't exist.");
			return;
		}
			
		node.removeFromHyPeerWeb();
		ObjectDB.getSingleton().remove(node.getLocalID());
		draw();
		System.out.println("Remove id:"+id+"\tnode.id:"+node.getWebId().getValue());
	}
	
	
	public void broadcast(int nodeID, String message)
	{
		if (hpw == null)
		{
			printToError("You can't broadcast a message because the segment hasn't been created yet.");
			return;
		}
		System.out.println("Send Broadcast ("+message+") from node "+nodeID+". : )");
		// Put code here from GUI Reed
		
		try
    	{
    		//int textValue = Integer.parseInt(startingNode.getText());
			Node start = hpw.getNode(nodeID);
			
    		if(start == null)
    		{
    			this.printToError("Broadcast id " + nodeID + " doesn't exist!");
    		}
    		else
    		{
    			
				//String message = messageBox.getText();
    			Parameters params = Broadcaster.createInitialParameters(message);
    			
				start.accept(new Broadcaster(), params);
    		}
    	}
    	catch(NumberFormatException e)
    	{
    		this.printToError("ID '" + nodeID+ "' isn't a number!");
    	}
		
	}
	public void sendMessage(int startID, int targetID,String  message)
	{
		if (hpw == null)
		{
			printToError("You can't send a message because the segment hasn't been created yet.");
			return;
		}
		String string = "Send Message ("+message+") from node "+startID+" to "+targetID+". : )";
		System.out.println();
		printToTracePanel(string);
		// Put code here from GUI, Reed
		
    	try
    	{
    		//int textValue = Integer.parseInt(startingNode.getText());
    		Node found = hpw.getNode(startID);
    		//int targetValue = Integer.parseInt(endingNode.getText());
    		if(found == null || !hpw.contains(new Node(targetID)))
    		{

				
				String missingID = "";
				
				if(found == null)
					missingID += startID;
				else
					missingID += targetID;
				
    			this.printToError("Id: " + missingID + " doesn't exist!");
    		}
    		else
    		{
//    			B. If successful, get the text in the "endingNode" component and convert it to an integer identifying the target node.
    	    	//			1. If the indicated target node is empty, or does not contain an integer, or does not identify an
    	    	//			existing node in the HyPeerWeb, post an error message in the "debugStatus" component of the GUI.
    		//	String message = messageBox.getText();
    			Parameters params = GUISender.createInitialParameters(targetID, message);
    			GUISender visitor = new GUISender();
    			found.accept(visitor, params);
    			// send message from start node to target node using "GUISENDER"
    		}
    	}
    	catch(NumberFormatException e) //		A. If the indicated start node is empty, or does not contain an integer
    	{
    		//, or does not identify an existing node in the HyPeerWeb, post an error message in the 
			//"debugStatus" component of the GUI.
    		this.printToError("ID '" + startID + "' isn't a number!");
    	}
	}
	
	public void draw()
	{
		if (hpw == null)
		{
			return; // Don't do anything.
		}
		
		java.util.ArrayList<Integer> nodes = getCurrentSegmentNodes();

		for (GUIProxy p : guis)		
			p.draw(nodes);
	}
	
	private java.util.ArrayList<Integer> getCurrentSegmentNodes()
	{
		/*int size = hpw.size();
		for (int i = 0; i < size; i++)
		{
			nodes.add(i);
		}*/
		ObjectDB db=ObjectDB.getSingleton();
		Enumeration<Object> list=db.enumeration();
		java.util.ArrayList<Integer> nodes = new java.util.ArrayList<Integer>();
		// ALL YOUR BASE ARE BELONG TO US!
		while(list.hasMoreElements())
		{
			Object o=list.nextElement();
			if(o instanceof Node && !(o instanceof NodeProxy))
			{
				Node n=(Node)o;
				nodes.add(n.getWebId().getValue());
			}

		}
		Collections.sort(nodes);
		return nodes;
	}
	
	/*
	public void kill()
	{
		int size = hpw.size();
		for(int i = 0; i < size; i++)
		{
			hpw.removeNode(hpw.getNode(i));
		}
		for(GUIProxy p : guis)
		{
			p.draw(new ArrayList<Integer>());
		}
		// kill GUI???
	}
	*/
	public void printToError(Object message)
	{
		for (GUIProxy p : guis)		
		{
			System.out.println(p+"printToError("+message+")");
			p.printToError(message);
		}
	}
	public void printToTracePanel(Object message)
	{
		for (GUIProxy p : guis)	{
			p.printToTracePanel(message);
		}
		
	}
	/*
	public void deleteAllNodes()
	{
		if (hpw == null)
		{
			printToError("You can't delete all nodes because the segment hasn't been created yet.");
			return;
		}
		System.out.println("Deleting all nodes! : )");
		
		int size = hpw.size();
		for(int i = 0; i < size; i++)
		{
			hpw.removeNode(hpw.getNode(i));
		}
		for(GUIProxy p : guis)
		{
			p.draw(new ArrayList<Integer>());
		}
	}
	*/
	public void shutdownHyPeerWeb()
	{
		
	}
	
	public void shutdownSegment()
	{
		if(hpw == null)
		{
			return;
		}
		
		System.out.println("Shutting down segment. : )");
		
		
		// Remove all nodes
		int size = hpw.size();
		for(int i = size - 1; i >=0; i--)
		{
			hpw.removeNode(hpw.getNode(i));
		}
		for(GUIProxy p : guis)
		{
			p.draw(new ArrayList<Integer>());
			p.printToError("");
			p.printToTracePanel("");
		}
	}
	
	public void saveToDBButton()
	{
		if (hpw != null)
			hpw.saveToDBButton();
	}
	
	public void loadFromDBButton()
	{
		if (hpw != null)
			hpw.loadFromDBButton();
	}
}
