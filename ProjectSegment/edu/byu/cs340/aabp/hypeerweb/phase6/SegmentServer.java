package edu.byu.cs340.aabp.hypeerweb.phase6;

import java.net.InetAddress;
import java.net.UnknownHostException;

import edu.byu.cs340.aabp.hypeerweb.SegmentManager;
import gui.Main.LocalObjectId;
import gui.Main.PortNumber;

public class SegmentServer {
	
	

	public static void main(String[] args) throws UnknownHostException
	{
		SegmentManager manager = SegmentManager.getSingleton();
		PortNumber port = new PortNumber(49200);
		LocalObjectId managerID = new LocalObjectId();
		ObjectDB.getSingleton().store(managerID, manager);
		
		System.out.println("Starting the HyPeerWebSegment Server.");
		System.out.println("IP:\t"+InetAddress.getLocalHost().getHostAddress());
		System.out.println("Port:\t"+port.getValue());
		System.out.println("Segment Manager Key:\t"+managerID+"\t(Use this to connect the GUI to the segment)");
				
		/*
        System.out.println("Server::main(String[]) ObjectDB = ");
        ObjectDB.getSingleton().dump();
        */
		
		try
		{
			PeerCommunicator.createPeerCommunicator(port);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}
	}

}
