package edu.byu.cs340.aabp.hypeerweb.phase6;

import gui.Main.GlobalObjectId;
import gui.Main.LocalObjectId;
import gui.Main.PortNumber;

public class BPClientTest {

	public static void main(String[] args) 
	{
		PeerCommunicator.createPeerCommunicator(new PortNumber(49201));
		GlobalObjectId serverGlobalObjectId = new GlobalObjectId("localhost",new PortNumber(49200),new LocalObjectId(-2147483648));
		/*
		SegmentManagerProxy proxy = new SegmentManagerProxy(serverGlobalObjectId);
		
		System.out.println("About to register...");
		
		GlobalObjectId myId = new GlobalObjectId();	
		
		System.out.println("myId:"+myId);
		
		proxy.register(new GlobalObjectId());
		*/
		System.out.println("Done");

		System.exit(0);
	}

}
