package edu.byu.cs340.aabp.hypeerweb.phase6;

public class Server {	
	public static void main(String[] args){
		ObjectDB.setFileLocation("Database.db");
		ObjectDB.getSingleton().restore(null);
        System.out.println("Server::main(String[]) ObjectDB = ");
        ObjectDB.getSingleton().dump();
		try
		{
			PeerCommunicator.createPeerCommunicator();
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}
	}
}
