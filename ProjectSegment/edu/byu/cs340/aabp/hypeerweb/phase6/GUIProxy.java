package edu.byu.cs340.aabp.hypeerweb.phase6;

import gui.Main.Command;
import gui.Main.GlobalObjectId;

public class GUIProxy
{
    private GlobalObjectId globalObjectId;

    public GUIProxy(GlobalObjectId globalObjectId){
        this.globalObjectId = globalObjectId;
    }


    public void draw(java.util.ArrayList<Integer> p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "java.util.ArrayList";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "gui.Main.GUI", "draw", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void kill(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "gui.Main.GUI", "kill", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void printToError(java.lang.Object p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "java.lang.Object";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "gui.Main.GUI", "printToError", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void printToTracePanel(java.lang.Object p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "java.lang.Object";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(), "gui.Main.GUI", "printToTracePanel", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public String toString()
    {
    	return globalObjectId.toString();
    }
	
    public boolean equals(GUIProxy obj) 
    {
    	System.out.println("GUIProxy.equals()");
    	return globalObjectId.equals(obj.globalObjectId);
    }


	public GlobalObjectId getGlobalId() 
	{
		return globalObjectId;
	}


	@Override
	public int hashCode() {
		return super.hashCode();
	}


	@Override
	public boolean equals(Object obj) {
    	return globalObjectId.equals(((GUIProxy)obj).globalObjectId);
	}
}
