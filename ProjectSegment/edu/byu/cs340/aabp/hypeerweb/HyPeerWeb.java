package edu.byu.cs340.aabp.hypeerweb;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

import edu.byu.cs340.aabp.hypeerweb.phase6.ObjectDB;

public abstract class HyPeerWeb
{
	static protected HyPeerWeb hpw = null;
	protected static HyPeerWebDatabase hdb;

	protected Node lastNode;
	
	
	protected HyPeerWeb()
	{
		lastNode = null;
		hdb = getHyPeerWebDatabase();
	}
	
	public void setLastNode(Node lastNode)
	{
		this.lastNode = lastNode;
	}
	
	/**
	 * @designer Tim
	 * @phase 5
	 * shut down gui
	 */
	public void close()
	{
		//@author Ben Save the DB
		saveToDatabase();
		// We assume it is clear
		// Destroy GUI
	//	clear();
	}
	
	abstract public void refreshEverything();
	
	/**
	 * @designer Tim
	 * @phase 5
	 * start up GUI
	 */
	public static void getHyPeerWeb()
	{
		// Make GUI???
		// Turn on GUI???
	}
		
	public void addNode(Node node)
	{
		if(node == null || node.equals(Node.NULL_NODE))
		{
			return;
		}
		
		Node referencePoint = getLastNode();
/*		
		System.out.println("HyPeerWeb.addNode() - referencePoint:"+referencePoint);
		
		if (referencePoint != null)
		{
			System.out.println(referencePoint.getClass());
			System.out.println(referencePoint.getWebId().getValue());
		}
	*/
		if (referencePoint == null)
		{
			node.setWebId(new WebId(0));
			lastNode = node;
		}
		else
		{
			referencePoint.addToHyPeerWeb(node);
			lastNode = node;
		}
	}
	
	abstract public Node getLastNode();
	
	public void clear() 
	{
		lastNode = null;
//		hdb.deleteEverythingFromDatabase();
	}
	
	public boolean contains(Node node)
	{
		return getNode(node.getWebId().getValue()) != null;
	}
	
	public HyPeerWebDatabase getHyPeerWebDatabase()
	{
		return HyPeerWebDatabase.getSingleton();
	}
	
	public Node getNode(int i) 
	{
		//tell lastNode to start searching for node
		Node found = null;
		if (lastNode != null)
		{
			found = lastNode.findToNode(i);
		}
		return found; 
	}

	static public HyPeerWeb getSingleton()
	{
		if (hpw == null)
		{
			System.err.println("Error! HyPeerWeb.getSingleton() with a null hypeerweb. Please call the getSingleton on the master or slave");
		}
		return hpw;
	}
	/*
	{
		if(hpw == null)
		{
			hpw = new HyPeerWeb();
			return hpw;
		}
		return hpw;
	}
	*/
	
	public void reload()
	{
		reload("hypeerweb1");
	}
	
	public void reload(java.lang.String dbName)
	{
		System.out.println("HyPeerWeb.reload()");
		HyPeerWebDatabase.initHyPeerWebDatabase();
		HyPeerWebDatabase hdb = getHyPeerWebDatabase();
		hdb.loadAllNodes();
		HyPeerWebDatabase.destroyHyPeerWebDatabase();
		SegmentManager.getSingleton().draw();
		
		/*
		HyPeerWebDatabase.initHyPeerWebDatabase(dbName);
		clear();
		
		HyPeerWebDatabase hdb = getHyPeerWebDatabase();
		ArrayList<Node> newNodeList = hdb.getAllNodes();
		
		lastNode = null;
		
		if (newNodeList.size() > 0)
		{
			lastNode = newNodeList.get(0);
		}
		*/
	}
	
	public abstract void saveToDatabase();
	/*
	{
		System.out.println("HyPeerWeb.saveToDatabase()");

		
		hdb.deleteEverythingFromDatabase();
		int size = size();
		for (int i = 0; i < size; i++)
		{
			hdb.saveNode(getNode(i));
		}
		
			
	}
	*/
	
	public void saveToDatabaseWorker()
	{
		System.out.println("HyPeerWeb.saveToDatabaseWorker()");
		
		Enumeration<Object> list=ObjectDB.getSingleton().enumeration();
		while(list.hasMoreElements())
		{
			Object o=list.nextElement();
			if(o instanceof Node && !(o instanceof NodeProxy))
			{
				hdb.saveNode((Node) o);
			}

		}
	}
	
	public void removeNode(Node node) 
	{
		if(lastNode.getWebId().getValue() == 0)
		{
			lastNode = null;
			node.removeFromHyPeerWeb();
		}
		else
		{
			Node bottom = node.greedyDown(node);	
			node.removeFromHyPeerWeb();
			
			//current bug, what if node == 0 node?
			//no matter what, last node will change in removeNode
			Node highestNode = bottom.greedyUp(bottom); 
			lastNode = highestNode;
		}
		
	}
	
	public int size() 
	{
		if (getLastNode() == null)
		{
			return 0;
		}
		return lastNode.getWebId().getValue() + 1;
	}
	
	//Pre-condition: newNode ≠ null AND startNode ≠ null
	//Why are we receiving bad input??
	public void addToHyPeerWeb(Node newNode, Node startNode) 
	{
		if(startNode==null)
		{
			//startNode=new Node(0);
			lastNode = newNode;
			return;
		}
		if(newNode==null)
		{
			return;
		}
		if(newNode.equals(Node.NULL_NODE))
		{
			return;
		}
		startNode.addToHyPeerWeb(newNode);
		lastNode = newNode;
	}
	
	abstract public void saveToDBButton();
	abstract public void loadFromDBButton();


}
