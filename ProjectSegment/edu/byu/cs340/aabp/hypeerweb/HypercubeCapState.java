/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.byu.cs340.aabp.hypeerweb;

/**
 *
 * @author rbirdman
 */
public class HypercubeCapState extends NodeState 
{
	public static final int STATE_ID = 3;
	
	public static final HypercubeCapState STATE_CLASS = new HypercubeCapState();
			
	private HypercubeCapState() 
	{
		
	}
	
	@Override
	public String toString() 
	{
		return "Hypercube Cap State";
	}
}
