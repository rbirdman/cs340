/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.byu.cs340.aabp.hypeerweb;

/**
 *
 * @author rbirdman
 */
public class UpPointingNodeState extends NodeState 
{
	public static final int STATE_ID = 1;
	
	public static final UpPointingNodeState STATE_CLASS = new UpPointingNodeState();
			
	private UpPointingNodeState() 
	{
		
	}
	
	@Override
	public String toString() 
	{
		return "Up Pointing Node State";
	}
}
