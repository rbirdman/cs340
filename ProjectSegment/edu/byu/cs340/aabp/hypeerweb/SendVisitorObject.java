package edu.byu.cs340.aabp.hypeerweb;
import edu.byu.cs340.aabp.hypeerweb.*;

/**
 * Test of SendVisitor.  Prints out webID of targetNode and all parameters.
 * @author Tim
 */
public class SendVisitorObject extends SendVisitor 
{
	@Override
	protected void targetOperation(Node node, Parameters parameters)
	{
		//System.out.println("I got to: ID="+node.getWebId().getValue());
		//System.out.println("Parameters:");
		//for(String key : parameters.keyset())
		//{
			//System.out.println("\t"+key+"="+parameters.get(key));
		//}
	}
	@Override
	protected void intermediateOperation(Node node, Parameters parameters)
	{
		assert(node != null && parameters != null);
		//Node target = (Node)parameters.get(TARGET_KEY);
		//System.out.println("Message Path: trying to reach "+ target.getWebId().getValue() + " Traveling through node "+ node.getWebId().getValue());
	}
}
