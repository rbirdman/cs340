/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.byu.cs340.aabp.hypeerweb;

/**
 *
 * @author rbirdman
 */
public abstract class NodeState 
{
//	private int STATE_ID;
	
	//Returns the NodeState whose STATE_ID corresponds to the id
	static NodeState getNodeState(int id) 
	{
		assert 0 <= id && id <= 4;
		
		switch(id) 
		{
			case StandardNodeState.STATE_ID: return StandardNodeState.STATE_CLASS;
			case UpPointingNodeState.STATE_ID: return UpPointingNodeState.STATE_CLASS;
			case DownPointingNodeState.STATE_ID: return DownPointingNodeState.STATE_CLASS;
			case HypercubeCapState.STATE_ID: return HypercubeCapState.STATE_CLASS;
			case TerminalNodeState.STATE_ID: return TerminalNodeState.STATE_CLASS;
			default: return null;
		}
	}
	
	@Override
	public abstract String toString();
}
