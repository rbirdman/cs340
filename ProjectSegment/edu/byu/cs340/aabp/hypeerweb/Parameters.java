package edu.byu.cs340.aabp.hypeerweb;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;

/**
 * 
 * A collection of objects that can be stored 
 * and used as parameters to a method that asks 
 * for it. Objects are stored as key and value 
 * pairs.
 * 
 * <pre>
 * <b>Domain:</b><br>
 *		parameters : Collection of key and value pairs. Key is the
 *			type of parameter desired and value is the actual parameter.
 *		TARGET_KEY : Key for getting a target node parameter
 *		MESSAGE_KEY : Key to retrieve messages from parameters
 * 
 * <b>Invariant:</b>
 *		parameters : Is not NULL
 *		KEYS : Be defined in parameters class to allow for public access
 *			for any class using Parameters class
 * 
 * </pre>
 * 
 * @author Ryan Bird
 */
public class Parameters implements Serializable 
{
	public static final String TARGET_KEY = "TARGET_KEY";
	public static final String MESSAGE_KEY = "message";
	
	private HashMap<String,Object> parameters;
	
	/**
	 * The default constructor.
	 * 
	 * @pre None
	 * @post Creates an empty collection of parameters.
	 */
	public Parameters()
	{
		parameters = new HashMap<String,Object>();
	}
	
	/**
	 * @designer Tim Atwood, Ryan Bird
	 * 
	 * @param target Value to instantiate TARGET_KEY
	 * @pre Target is ID of a Node within a HyPeerWeb
	 * @post Creates a collection of parameters with TARGET_KEY mapping set to<br>
	 *		a Node class with an id equal to 'target'.
	 */
	public Parameters(int target)
	{
		this();
		
		Node targetNode = new Node(target);
		set(Parameters.TARGET_KEY, targetNode);
	}
	
	/**
	 * I'm just building off of Tim's set up in order to include a message as well.
	 * 
	 * @designer Ryan Bird
	 * @param target WebID of the node to send to.
	 * @param message The message to give that specific node.
	 * 
	 * @pre target is the ID of a valid Node id, message is the desired message to send
	 * @post Initializes a collect of parameters with TARGET_KEY and MESSAGE_KEY set to<br>
	 *		a Node object with 'target' id and the given String message.
	 */
	public Parameters(int target, String message)
	{
		this(target);
		set(Parameters.MESSAGE_KEY, message);
	}
	
	/**
	 * Returns the number of elements stored in Parameters
	 * 
	 * @designer Ryan Bird
	 * 
	 * @pre None
	 * @post None
	 * @return Number of parameters stored in collection
	 */
	public int length()
	{
		return parameters.size();
	}
	
	/**
	 * Sets the given key to map to the given value.
	 * 
	 * @designer Ryan Bird
	 * 
	 * @param key Key to map to given value.
	 * @param value Object to store in the collection.
	 * @pre key and value are not equal to null
	 * @post key now maps to given value. Old mapping will be overwritten.
	 */
	public void set(String key, Object value) 
	{
		if (key == null)
		{
			return;
		}
		parameters.put(key,value);
	}
	
	/**
	 * Returns the value associated with the given key.
	 * 
	 * @designer Ryan Bird
	 * 
	 * @pre A value is already stored with the associated key.
	 * @post None
	 * @param key Key used to store the object that is being requested.
	 * @return Object that is mapped by the given key.
	 */
	public Object get(String key)
	{
		return parameters.get(key);
	}
	
	/**
	 * Returns the set of keys that are stored in this collection of parameters.
	 * 
	 * @designer Ryan Bird
	 * 
	 * @pre None
	 * @post None
	 * @return Set of Keys associated with these parameters.
	 */
	public Set<String> keyset()
	{
		return parameters.keySet();
	}
	
	/**
	 * Petitions whether these parameters contain a particular key.
	 * 
	 * @designer Ryan Bird
	 * 
	 * @pre key != null
	 * @post None
	 * @param key Key in question
	 * @return Boolean value that determines whether or not parameters contain certain key.
	 */
	public boolean containsKey(String key)
	{
		return parameters.containsKey(key);
	}
}
