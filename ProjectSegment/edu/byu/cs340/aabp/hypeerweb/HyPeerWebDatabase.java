/*
CS340 HyPeerWeb
Benjamin Parry
*/
package edu.byu.cs340.aabp.hypeerweb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import edu.byu.cs340.aabp.hypeerweb.phase6.MachineAddress;
import edu.byu.cs340.aabp.hypeerweb.phase6.ObjectDB;
import gui.Main.GlobalObjectId;
import gui.Main.LocalObjectId;
import gui.Main.PortNumber;

public class HyPeerWebDatabase {
	
	static private HyPeerWebDatabase hypeerwebdatabase = null;
	
	private Connection con;
	
	private static String DATABASE_DIRECTORY; // server url
	public final static String DEFAULT_DATABASE_NAME_DEFAULT = "hypeerweb1"; // schema
	private static String DEFAULT_DATABASE_NAME; // schema
	private static String DEFAULT_DATABASE_USERNAME;
	private static String DEFAULT_DATABASE_PASSWORD;
	
	// Constructor is private to make it a singleton
	private HyPeerWebDatabase()
	{
		DATABASE_DIRECTORY = "jdbc:mysql://db1.cs340.parry.co:443";
		DEFAULT_DATABASE_NAME = DEFAULT_DATABASE_NAME_DEFAULT;
		DEFAULT_DATABASE_USERNAME = "hypeerwebuser";
		DEFAULT_DATABASE_PASSWORD = "hypeerwebpassword340";
		
	}
	public static void reset()
	{
		DEFAULT_DATABASE_NAME = DEFAULT_DATABASE_NAME_DEFAULT;
	}
	
	void removeNode(Node node)
	{
		if (node == null || node.equals(Node.NULL_NODE))
		{
			return;
		}
		
		System.out.println("removeNode is not implemented");
		/*
		int webID = node.getWebId().getValue();
		deleteNodeFromDatabase(webID);
		
		deleteNeighborsFromDatabase(webID);
		
		deleteSurrogateNeighborsFromDatabase(node.getWebId().getValue());
		*/
	}
	
	
	void saveNode(Node node)
	{
		if (node == null || node.equals(Node.NULL_NODE))
		{
			return;
		}
		
		int webID = node.getWebId().getValue();
		deleteNodeFromDatabase(webID);
		
		try 
		{
			executeUpdate("insert into Node (webID, foldNodeID, surrogateFoldNodeID, inverseSurrogateFoldNodeID, height, ip, port, localID) values " +
					"('"+webID+"','"+node.getFold().getWebId().getValue()+"'," +
							"'"+node.getSurrogateFold().getWebId().getValue()+"','"+node.getSurrogateFold().getWebId().getValue()+"'," +
									"'"+node.getWebId().getHeight()+"','"+MachineAddress.getThisMachinesInetAddress().getHostAddress()+"','"+PortNumber.getApplicationsPortNumber()+"','"+node.getLocalID().getId()+"');");
		} catch (Exception e) 
		{
			System.err.println("Error inserting node:"+node.getWebId().getValue());
			e.printStackTrace();
		}
		
		//Neighbor
		deleteNeighborsFromDatabase(webID);
		for (Node neighborID : node.getNeighbors())
		{
			try 
			{
				executeUpdate("insert into Neighbor (nodeID, neighborNodeID) values " +
						"('"+webID+"','"+neighborID.getWebId().getValue()+"');");
			} catch (Exception e) 
			{
				System.err.println("Error inserting neighbor nodeID:"+webID);
				e.printStackTrace();
			}
		}
		
		//SurrogateNeighbor
		deleteSurrogateNeighborsFromDatabase(node.getWebId().getValue());
		for (Node neighborID : node.getSurrogateNeighbors())
		{
			try 
			{
				executeUpdate("insert into SurrogateNeighbor (nodeID, surrogateNeighborNodeID) values " +
						"('"+webID+"','"+neighborID.getWebId().getValue()+"');");
			} catch (Exception e) 
			{
				System.err.println("Error inserting surrogate neighbor nodeID:"+webID);
				e.printStackTrace();
			}
		}
			
	}
	
	void deleteEverythingFromDatabase()
	{
		try 
		{
			executeUpdate("delete from Neighbor;");
		} catch (Exception e) {
			System.err.println("Error deleting all neighbors");
			e.printStackTrace();
		}		
		
		try 
		{
			executeUpdate("delete from Node;");
		} catch (Exception e) {
			System.err.println("Error deleting all nodes");
			e.printStackTrace();
		}
		
		try 
		{
			executeUpdate("delete from SurrogateNeighbor;");
		} catch (Exception e) {
			System.err.println("Error deleting all neighbors");
			e.printStackTrace();
		}
		
	}
	
	void deleteNeighborsFromDatabase (int webID)
	{
		try 
		{
			executeUpdate("delete from Neighbor where nodeID='"+webID+"';");
		} catch (Exception e) {
			System.err.println("Error deleting neighbors:"+webID);
			e.printStackTrace();
		}
	}
	
	void deleteSurrogateNeighborsFromDatabase (int webID)
	{
		try 
		{
			executeUpdate("delete from SurrogateNeighbor where nodeID='"+webID+"';");
		} catch (Exception e) {
			System.err.println("Error deleting neighbors:"+webID);
			e.printStackTrace();
		}
	}
	
	void deleteNodeFromDatabase (int webID)
	{
		try 
		{
			executeUpdate("delete from Node where webID='"+webID+"';");
		} catch (Exception e) {
			System.err.println("Error deleting node:"+webID);
			e.printStackTrace();
		}
	}
	
	//Creates a SimplifiedNodeDomain representing the node with indicated webId.
	SimplifiedNodeDomain getNode(int webId) 
	{
		return getNodeByID(webId).constructSimplifiedNodeDomain();
	}
	
	private Node getNodeByID(int webID)
	{
		return getNodeByQuery("select * from Node n where webID='"+webID+"';");
	}
	
	private Node getNodeByQuery(String query)
	{
		ArrayList<Node> nodes = getNodesByQuery(query);
		if (nodes.size() > 0)
		{
			return nodes.get(0);
		}
		else
		{
			return Node.NULL_NODE;
		}
	}
	
	public ArrayList<Node> loadAllNodes()
	{
		ArrayList<Node> toReturnList = getNodesByQuery("select * from Node n;");
		
		HashMap<Integer, Node> nodes = new HashMap<Integer, Node> ();
		nodes.put(-1,Node.NULL_NODE);
		
		for (Node toReturn : toReturnList)
		{
			nodes.put((Integer) toReturn.tempWebId, toReturn);
			System.out.println("loadAllNodes\t"+toReturn.tempWebId+"\t"+toReturn.getClass());
		}
		
		for (Node toReturn : toReturnList)
		{
			if (!toReturn.getClass().equals(NodeProxy.class))
			{
				ObjectDB.getSingleton().store(toReturn.getLocalID(), toReturn);
				LocalObjectId.setLastUsed(toReturn.getLocalID().getId());
				HyPeerWeb.getSingleton().setLastNode(toReturn);
				
				int id = toReturn.getWebId().getValue();
				toReturn.setFold(nodes.get(getIntByQuery("select foldNodeID from Node n where webID='"+id+"';"))); 
				toReturn.setSurrogateFold(nodes.get(getIntByQuery("select surrogateFoldNodeID from Node n where webID='"+id+"';"))); 
				toReturn.setInverseSurrogateFold(nodes.get(getIntByQuery("select inverseSurrogateFoldNodeID from Node n where webID='"+id+"';"))); 

				//Neighbor
				String query = "select neighborNodeID from Neighbor where nodeID='"+id+"';";
				for (Integer i : getIntsByQuery(query))
				{
					toReturn.addNeighbor(nodes.get(i),false);
				}

				//SurrogateNeighbor
				query = "select surrogateNeighborNodeID from SurrogateNeighbor where nodeID='"+id+"';";
				for (Integer i : getIntsByQuery(query))
				{
					toReturn.addSurrogateNeighbor(nodes.get(i));
				}

				//SurrogateNeighbor
				query = "select nodeID from SurrogateNeighbor where surrogateNeighborNodeID='"+id+"';";
				for (Integer i : getIntsByQuery(query))
				{
					toReturn.addUpPointer(nodes.get(i));
				}
			}
		}
		
		return toReturnList;
		
	}

	private ArrayList<Node> getNodesByQuery(String query)
	{
		Statement statement = null;
		ResultSet resultset = null;
		try
		{
			statement = getStatement();
			resultset = statement.executeQuery(query);
			ArrayList<Node> toReturn = makeArrayNode(resultset);
			resultset.close();
			statement.close();
			return toReturn;
			
		}catch (Exception e)
		{
			System.err.println("Error executing query:\""+query+"\"");
			e.printStackTrace();
			try
			{
				resultset.close();
			} catch (Exception ee){}
			try
			{
				statement.close();
			} catch (Exception ee){}
		}
		return new ArrayList<Node>();
	}	
	
	private Node generateNode(ResultSet resultset, String title) throws Exception
	{
		
		int webID = resultset.getInt(title+".webID");
		GlobalObjectId globalID = new GlobalObjectId(resultset.getString(title+".ip"), 
				new PortNumber(resultset.getInt(title+".port")), new LocalObjectId(resultset.getInt(title+".localID")));
		
		Node toReturn = null;
		
		if (globalID.onSameMachineAs(new GlobalObjectId()))
		{
			toReturn = new Node(webID);
			toReturn.setLocalID(globalID.getLocalObjectId());
		}
		else
		{
			toReturn = new NodeProxy(globalID);
		}
		
		toReturn.tempWebId = webID;
		
		
		return toReturn;
		
	}
	private ArrayList<Node> makeArrayNode(ResultSet resultset) throws Exception
	{
		return makeArrayNode(resultset,"n");
	}
	private ArrayList<Node> makeArrayNode(ResultSet resultset, String title) throws Exception
	{
		ArrayList<Node> toReturn = new ArrayList<Node>();
		resultset.beforeFirst();
		while(resultset.next())
		{
			toReturn.add(generateNode(resultset,title));
		}
		return toReturn;
	}
	
	private ArrayList<Integer> getIntsByQuery(String query)
	{
		Statement statement = null;
		ResultSet resultset = null;
		try
		{
			statement = getStatement();
			resultset = statement.executeQuery(query);
			ArrayList<Integer> toReturn = new ArrayList<Integer>();
			resultset.beforeFirst();
			while(resultset.next())
			{
				toReturn.add(resultset.getInt(1));
			}
			resultset.close();
			statement.close();
			return toReturn;
			
		}catch (Exception e)
		{
			System.err.println("Error executing query:\""+query+"\"");
			e.printStackTrace();
			try
			{
				resultset.close();
			} catch (Exception ee){}
			try
			{
				statement.close();
			} catch (Exception ee){}
		}
		return new ArrayList<Integer>();
	}
	
	private int getIntByQuery(String query)
	{
		Statement statement = null;
		ResultSet resultset = null;
		try
		{
			statement = getStatement();
			resultset = statement.executeQuery(query);
			resultset.beforeFirst();
			int toReturn = -1;
			while(resultset.next())
			{
				toReturn = (resultset.getInt(1));
			}
			resultset.close();
			statement.close();
			return toReturn;
			
		}catch (Exception e)
		{
			System.err.println("Error executing query:\""+query+"\"");
			e.printStackTrace();
			try
			{
				resultset.close();
			} catch (Exception ee){}
			try
			{
				statement.close();
			} catch (Exception ee){}
		}
		return -1;
	}
	
	public int executeUpdate (String s) throws Exception
	{
		try
		{
			Statement statement = getStatement();
			int toReturn = statement.executeUpdate(s);
			statement.close();
			return toReturn;
		}
		catch (Exception e)
		{
			try
			{
				DisconnectFromDatabaseServer();
				ConnectToDatabaseServer();
				Statement state = con.createStatement();
				int toReturn = state.executeUpdate(s);
				state.close();
				return toReturn;
			}
			catch (Exception ee)
			{
				throw e;
			}
		}
	}
	
	
	//Gets the single HyPeerWebDatabase.
	public static HyPeerWebDatabase getSingleton() 
	{
		if (hypeerwebdatabase == null)
		{
			hypeerwebdatabase = new HyPeerWebDatabase();
		}
		return hypeerwebdatabase;
	}
	
	//Creates and loads a HyPeerWebDatabase.
	static void initHyPeerWebDatabase(java.lang.String dbName) 
	{
		DEFAULT_DATABASE_NAME = dbName;
		destroyHyPeerWebDatabase();
		getSingleton().ConnectToDatabaseServer();
	}
	
	public static void initHyPeerWebDatabase() 
	{
		destroyHyPeerWebDatabase();
		getSingleton().ConnectToDatabaseServer();
	}
	
	public Statement getStatement() throws Exception
	{
		try
		{
			return con.createStatement();
		}
		catch (Exception e)
		{
			try
			{
				DisconnectFromDatabaseServer();
				ConnectToDatabaseServer();
				return con.createStatement();
				
			}catch (Exception ee)
			{
				throw e;
			}
		}
	}
	
	static void destroyHyPeerWebDatabase()
	{
		try
		{
			hypeerwebdatabase.DisconnectFromDatabaseServer();
		} catch (Exception e) {}
		
	}
	
	private synchronized void ConnectToDatabaseServer()
	{
		System.out.println("HyPeerWeebDatabse.ConnectToDatabaseServer()");
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(DATABASE_DIRECTORY + "/" + DEFAULT_DATABASE_NAME+"?autoReconnect=true", DEFAULT_DATABASE_USERNAME, DEFAULT_DATABASE_PASSWORD);
		} catch (Exception e) {
			System.err.println("Can't connect to database:"+DATABASE_DIRECTORY+", schema:"+DEFAULT_DATABASE_NAME
					+", username:"+DEFAULT_DATABASE_USERNAME);
			e.printStackTrace();
		}	
	}
	
	private synchronized void DisconnectFromDatabaseServer()
	{
		System.out.println("HyPeerWeebDatabse.DisconnectFromDatabaseServer()");

		try
		{
			con.close();
		}
		catch (Exception e)
		{
		}
		con = null;
	}
	
	public void clear() 
	{
		//TODO Clear out the tables on the DB.
	}
	
}
