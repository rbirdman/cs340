/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.byu.cs340.aabp.hypeerweb;

/**
 *
 * @author rbirdman
 */
public class StandardNodeState extends NodeState 
{
	public static final int STATE_ID = 0;
	
	public static final StandardNodeState STATE_CLASS = new StandardNodeState();
			
	protected StandardNodeState() 
	{
		
	}
	
	@Override
	public String toString() 
	{
		return "Standard Node State";
	}
}
