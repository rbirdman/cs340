package edu.byu.cs340.aabp.hypeerweb;

public class DisconnectSecondToLastNode extends DisconnectTemplate {

	@Override
	protected void subDisconnect(Node thisNode) 
	{
		thisNode.getFold().setFold(thisNode.getFold());

	}

}
