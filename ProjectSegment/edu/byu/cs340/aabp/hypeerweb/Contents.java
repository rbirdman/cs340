package edu.byu.cs340.aabp.hypeerweb;

import java.io.Serializable;
import java.util.HashMap;
/**
 * The Contents of a node in the HyPeerWeb.<br>
 * <br>
 * 
 * <pre>
 * <b>Domain:</b>
 *      contents : HashMap<String,Object>
 * 
 * <b>Invariant:</b>
 *      contents; NOT NULL
 *      
 * -- In other words:
 * -- contents cannot be null or the key cannot be null.
 * </pre>
 * 
 * @author Reed Allred
 */
public class Contents implements Serializable
{
    // Domain Implementation
    /**
     * The HashMap for this Contents.
     */
	HashMap<String,Object> contents;
	
    // Constructors
    /**
     * The default constructor.
     * 
     * @pre <i>None</i>
     * @post contents not null
     */
	public Contents()
	{
		contents = new HashMap<String,Object>();
	}
	
    /**
     * The containsKey.
     * @param key a key that maps to a value
     * @pre <i>Not null</i>
     * @post true or false
     */
	public boolean containsKey(java.lang.String key) 
	{
		if(key==null)
		{
			return false;
		}
		return contents.containsKey(key);
	}
	
    /**
     * The set.
     * @param key a key that will map to the value
     * @param value the value that the key will map to  
     * @pre <i>key not null</i>
     * @post if key is null nothing is set, otherwise set key and value
     */
	public void set(String key, Object value) 
	{
		if (key == null)
		{
			return;
		}
		contents.put(key,value);
	}
	
    /**
     * The get.
     * @param key a key that will map to the value  
     * @pre <i>key not null</i>
     * @post if key is null nothing null is returned, otherwise the value of that key map is returned
     */
	public Object get(String key)
	{
		if(key==null) // might need to check if the key exists in conents
		{ 
			return null;
		}
		return contents.get(key);
	}
}
