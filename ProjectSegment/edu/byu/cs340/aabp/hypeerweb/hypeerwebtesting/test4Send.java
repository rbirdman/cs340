package edu.byu.cs340.aabp.hypeerweb.hypeerwebtesting;
import edu.byu.cs340.aabp.hypeerweb.*;

public class test4Send extends tester
{
	@Override
	public void test() 
	{
		System.out.println("\nTesting Phase 4 SendVisitor");
        hypeerweb.clear();
        errorCount = 0;
        int passCount = 0;
        SendVisitorObject visitor = new SendVisitorObject();
        
        boolean deletionError = false;
        
        System.out.print("    ");
        
        for (int size = 1; size <= HYPEERWEB_SIZE; size++)
        {
            if(VERBOSE) 
            {
                System.out.println("Testing SendVisitor on HyPeerWeb of size " + size + "/" + HYPEERWEB_SIZE);
            } 
            else
            {
                System.out.print(size + " ");
            }
            
            hypeerweb.clear();
            Node node0 = new Node(0);
            hypeerweb.addToHyPeerWeb(node0, null);
            Node firstNode = hypeerweb.getNode(0);
            
            Parameters parameters = new Parameters();
            for (int i = 0; i < size; i++) 
            {
                createHyPeerWebWith(size);

                for(int j = size - 1; j > 0; j--)
                {
                	System.out.println("From " + i + " to " + j );
                	parameters = visitor.createInitialParameters(j);
                	int numberOfHops = 0;
                	boolean reached = false;
                	parameters.set("HOPS", numberOfHops);
                	parameters.set("REACHED", reached);
                	firstNode = hypeerweb.getNode(i);
                	firstNode.accept(visitor, parameters);
                	System.out.println();
                	System.out.println("From " + j + " to " + i);
                	parameters = visitor.createInitialParameters(i);
                	firstNode = hypeerweb.getNode(j);
                	firstNode.accept(visitor, parameters);
                	System.out.println();
                }
                
            }
        }
        if(!VERBOSE)
        {
            System.out.println();
        }
        
        if (deletionError) 
        {
            double decimalPercent = ((double)(passCount) / (double) (passCount + errorCount));
            System.out.println("    Passes:"+passCount+"\tErrors:"+errorCount);
            int integerPercent = (int) (decimalPercent * 100.0d);
            System.out.println("    SendVisitor Error: Phase 4 Score = " + integerPercent + "%");
        }
        else
        {
            System.out.println("    No SendVisitor Errors: Phase 4 Score = 100%");
        }
	}
}
