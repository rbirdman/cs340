package edu.byu.cs340.aabp.hypeerweb.hypeerwebtesting;
import edu.byu.cs340.aabp.hypeerweb.*;

public class test3 extends tester
{

	@Override
	public void test() 
	{
		System.out.println("\nTesting Phase 3");
        hypeerweb.clear();
        errorCount = 0;
        int passCount = 0;
		/*
		 * For testing, modify the Exp.java class to completely test the removal of any node from a HyPeerWeb of size 32 or smaller. This should produce about 528 test cases. The code should be very similar to the code in the Exp.java class for testing insertion.
		 */
//		throw new UnsupportedOperationException("We need to implement the remove tests");
		
        boolean deletionError = false;
        System.out.print("    ");
        
        for (int size = 1; size <= HYPEERWEB_SIZE; size++) {
            if(VERBOSE) {
                System.out.println("Testing Node Deletion on HyPeerWeb of size " + size + "/" + HYPEERWEB_SIZE);
            } else {
                System.out.print(size + " ");
            }
            
            hypeerweb.clear();
            Node node0 = new Node(0);
            hypeerweb.addToHyPeerWeb(node0, null);
            Node firstNode = hypeerweb.getNode(0);
            SimplifiedNodeDomain simplifiedNodeDomain = firstNode.constructSimplifiedNodeDomain();
            ExpectedResult expectedResult = new ExpectedResult(1, 0);

            if (!simplifiedNodeDomain.equals(expectedResult)) {
            	deletionError = true;
                printErrorMessage("DELETION",size, null, simplifiedNodeDomain, expectedResult);
            }
            
            for (int deleteNodeId = 0; deleteNodeId < size; deleteNodeId++) {
                createHyPeerWebWith(size);
                
                Node deleteNode = hypeerweb.getNode(deleteNodeId);
                deleteNode.removeFromHyPeerWeb();
                
                for (int i = 0; i < size-1; i++) {
                    Node nodei = hypeerweb.getNode(i);
                    simplifiedNodeDomain = nodei.constructSimplifiedNodeDomain();
                    expectedResult = new ExpectedResult(size-1, i);

                    if (!simplifiedNodeDomain.equals(expectedResult)) 
                    {
                        deletionError = true;
                        printErrorMessage("DELETION",size, deleteNode, simplifiedNodeDomain, expectedResult);
                    }
                    else
                    {
                    	passCount ++;
                    }
                }
            }
        }
        if(!VERBOSE){
            System.out.println();
        }
        
        if (deletionError) {
            double decimalPercent = ((double)(passCount) / (double) (passCount + errorCount));
            System.out.println("    Passes:"+passCount+"\tErrors:"+errorCount);
            int integerPercent = (int) (decimalPercent * 100.0d);
            System.out.println("    Deletion Error: Phase 3 Score = " + integerPercent + "%");
        } else {
            System.out.println("    No Deletion Errors: Phase 3 Score = 100%");
        }
		
		
	}

}
