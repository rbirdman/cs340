package edu.byu.cs340.aabp.hypeerweb.hypeerwebtesting;
import edu.byu.cs340.aabp.hypeerweb.*;

public class AddTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		/*
		int start = 0;
		int to = 22;
		int next = start;
		do
		{
			next = findNextUp(next,to);
		} while (next != to);
		
		
		int TEST_MAX = 8;
		Node[] nodes = new Node[TEST_MAX];
		for (int i = 0; i < TEST_MAX; i++)
			nodes[i] = new Node(i);
		
		System.out.println("Create all nodes...");
		printNodes(nodes,1);
		
		System.out.println("Assume 0 is the parent. Add 1.");
		nodes[0].addToHyPeerWeb(nodes[1]);
		printNodes(nodes,2);

		System.out.println("Assume 0 is the parent. Add 2.");
		nodes[0].addToHyPeerWeb(nodes[2]);
		printNodes(nodes,3);	

		System.out.println("Assume 1 is the parent. Add 3.");
		nodes[1].addToHyPeerWeb(nodes[3]);
		printNodes(nodes,4);		
		
		System.out.println("Assume 0 is the parent. Add 4.");
		nodes[0].addToHyPeerWeb(nodes[4]);
		printNodes(nodes,5);	
		
		System.out.println("Assume 1 is the parent. Add 5.");
		nodes[1].addToHyPeerWeb(nodes[5]);
		printNodes(nodes,6);	
		*/
		
		
		
		HyPeerWeb web = HyPeerWeb.getSingleton();
		
		int TEST_MAX = 8;
		Node[] nodes = new Node[TEST_MAX];
		for (int i = 0; i < TEST_MAX; i++)
			nodes[i] = new Node(i);

		web.addNode(nodes[0]);
		web.addNode(nodes[1]);
		printNodes(nodes,2);	
		web.addNode(nodes[2]);
		printNodes(nodes,3);	
		web.addNode(nodes[3]);
		printNodes(nodes,4);	

	}
	
	public static void printNodes(Node[] nodes, int numberToPrint)
	{
		for (int i = 0; i < nodes.length && i < numberToPrint; i++)
			System.out.println("Node "+i+":\n"+nodes[i].constructSimplifiedNodeDomain().toString());
		System.out.println("");
	}
	
	public static int findNextUp(int current, int toGetTo)
	{
		int xor = current ^ toGetTo;
		int bit = 1;
		while ((bit & xor) != bit)
			bit = bit * 2;
			
		int toReturn = current ^ bit;
		
		System.out.println("current: "+current+"\ttoGetTo"+toGetTo+"\tnextHop:"+toReturn);
		return toReturn;
	}

}
