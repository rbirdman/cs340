package edu.byu.cs340.aabp.hypeerweb.hypeerwebtesting;
import edu.byu.cs340.aabp.hypeerweb.*;

public class test2 extends tester
{

	@Override
	public void test() 
	{
	    System.out.println("\nTesting Phase 2");
        hypeerweb.clear();
        errorCount = 0;
        
        boolean insertionError = false;
        System.out.print("    ");
        
        for (int size = 1; size <= HYPEERWEB_SIZE; size++) {
            if(VERBOSE) {
                System.out.println("Testing Node Insertion on HyPeerWeb of size " + size + "/" + HYPEERWEB_SIZE);
            } else {
                System.out.print(size + " ");
            }
            
            hypeerweb.clear();
            Node node0 = new Node(0);
            hypeerweb.addToHyPeerWeb(node0, null);
            Node firstNode = hypeerweb.getNode(0);
            SimplifiedNodeDomain simplifiedNodeDomain = firstNode.constructSimplifiedNodeDomain();
            ExpectedResult expectedResult = new ExpectedResult(1, 0);

            if (!simplifiedNodeDomain.equals(expectedResult)) {
                insertionError = true;
                printErrorMessage("INSERTION",size, null, simplifiedNodeDomain, expectedResult);
            }
            
            for (int startNodeId = 0; startNodeId < size - 1; startNodeId++) {
                createHyPeerWebWith(size - 1);
                
                Node node = new Node(0);
                Node startNode = hypeerweb.getNode(startNodeId);
                hypeerweb.addToHyPeerWeb(node, startNode);
                
                for (int i = 0; i < size; i++) {
                    Node nodei = hypeerweb.getNode(i);
                    simplifiedNodeDomain = nodei.constructSimplifiedNodeDomain();
                    expectedResult = new ExpectedResult(size, i);

                    if (!simplifiedNodeDomain.equals(expectedResult)) {
                        insertionError = true;
                        printErrorMessage("INSERTION",size, startNode, simplifiedNodeDomain, expectedResult);
                    }
                }
            }
        }
        if(!VERBOSE){
            System.out.println();
        }
        
        if (insertionError) {
            double decimalPercent = ((double)PHASE_2_TEST_COUNT - (double) errorCount) / (double) PHASE_2_TEST_COUNT;
            int integerPercent = (int) (decimalPercent * 100.0d);
            System.out.println("    Insertion Error: Phase 2 Score = " + integerPercent + "%");
        } else {
            System.out.println("    No Insertion Errors: Phase 2 Score = 100%");
        }  
		
	}

}
