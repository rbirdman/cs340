package edu.byu.cs340.aabp.hypeerweb.hypeerwebtesting;
import edu.byu.cs340.aabp.hypeerweb.*;

import java.util.ArrayList;


public class tester 
{
	public void run(tester test)
	{
		HyPeerWebDatabase.initHyPeerWebDatabase();
	    HyPeerWebDatabase.getSingleton().clear();
	    hypeerweb = HyPeerWeb.getSingleton();
		test.test();
	}
	
	tester()
	{
		
	}
	
	public void test()
	{	    
	}
	public static HyPeerWeb hypeerweb = null;
    protected static final int PHASE_1_TEST_COUNT = 66;
    protected static final int PHASE_2_TEST_COUNT = 10912;
    protected static final int HYPEERWEB_SIZE = 32;
    protected static final boolean VERBOSE = true;

    protected static int errorCount = 0;
    
    protected static boolean testAddingAndRemovingNeighbors(Node node,
            boolean error) {
        Node node0 = new Node(0);
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        
        Node neighbor0 = node0;
        Node neighbor1 = node1;
        Node neighbor2 = node2;

        node.addNeighbor(neighbor0, true);
        SimplifiedNodeDomain simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getNeighbors().size() != 1) {
            System.err.println("When invoking addNeighbor(Node) on a node with no neighbors the number of neighbors should be 1 but is "    + simpleDomain.getNeighbors().size() + ".");
        }
        if (!simpleDomain.getNeighbors().contains(0)) {
            System.err.println("When invoking addNeighbor(Node) on a node with no neighbors node 1 should be a neighbor but is not.");
        }

        node.addNeighbor(neighbor1, true);
        node.addNeighbor(neighbor2, true);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getNeighbors().size() != 3) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node, the number of neighbors should be 3 but is "    + simpleDomain.getNeighbors().size() + ".");
        }
        if (!simpleDomain.getNeighbors().contains(0)) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node, node 0 should be a neighbor but is not.");
        }
        if (!simpleDomain.getNeighbors().contains(1)) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node, node 1 should be a neighbor but is not.");
        }
        if (!simpleDomain.getNeighbors().contains(2)) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node, node 2 should be a neighbor but is not.");
        }

        node.removeNeighbor(neighbor0);
        node.removeNeighbor(neighbor2);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getNeighbors().size() != 1) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node and then removing node0 and node2 as neighbors, the number of neighbors should be 1 but is " +
                               simpleDomain.getNeighbors().size() + ".");
        }
        if (simpleDomain.getNeighbors().contains(0)) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node and then removing node0 and node2 as neighbors, node 0 should NOT be a neighbor but is.");
        }
        if (simpleDomain.getNeighbors().contains(2)) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node and then removing node0 and node2 as neighbors, node 2 should NOT be a neighbor but is.");
        }
        if (!simpleDomain.getNeighbors().contains(1)) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node and then removing node0 and node2 as neighbors, node 1 should be a neighbor but is NOT.");
        }

        node.removeNeighbor(neighbor1);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getNeighbors().size() != 0) {
            System.err.println("After adding node0, node1, and node2 as neighbors to node and then removing node0, node1, and node2 as neighbors, " +
                               "the number of neighbors should be 0 but is "    + simpleDomain.getNeighbors().size());
        }

        return error;
    }
    
    protected static boolean testAddingAndRemovingUpPointers(Node node,
            boolean error) {
        Node node0 = new Node(0);
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        
        Node upPointer0 = node0;
        Node upPointer1 = node1;
        Node upPointer2 = node2;

        node.addUpPointer(upPointer0);
        SimplifiedNodeDomain simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getUpPointers().size() != 1) {
            System.err.println("When invoking addUpPointer(Node) on a node with no upPointers the number of upPointers should be 1 but is "    + simpleDomain.getUpPointers().size() + ".");
        }
        if (!simpleDomain.getUpPointers().contains(0)) {
            System.err.println("When invoking addUpPointer(Node) on a node with no upPointers node 1 should be an upPointer but is not.");
        }

        node.addUpPointer(upPointer1);
        node.addUpPointer(upPointer2);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getUpPointers().size() != 3) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node, the number of upPointers should be 3 but is "    + simpleDomain.getUpPointers().size() + ".");
        }
        if (!simpleDomain.getUpPointers().contains(0)) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node, node 0 should be a upPointer but is not.");
        }
        if (!simpleDomain.getUpPointers().contains(1)) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node, node 1 should be a upPointer but is not.");
        }
        if (!simpleDomain.getUpPointers().contains(2)) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node, node 2 should be a upPointer but is not.");
        }

        node.removeUpPointer(upPointer0);
        node.removeUpPointer(upPointer2);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getUpPointers().size() != 1) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node and then removing node0 and node2 as upPointers, the number of upPointers should be 1 but is " +
                               simpleDomain.getUpPointers().size());
        }
        if (simpleDomain.getUpPointers().contains(0)) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node and then removing node0 and node2 as upPointers, node 0 should NOT be a upPointer but is.");
        }
        if (simpleDomain.getUpPointers().contains(2)) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node and then removing node0 and node2 as upPointers, node 2 should NOT be a upPointer but is.");
        }
        if (!simpleDomain.getUpPointers().contains(1)) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node and then removing node0 and node2 as upPointers, node 1 should be a upPointer but is NOT.");
        }

        node.removeUpPointer(upPointer1);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getUpPointers().size() != 0) {
            System.err.println("After adding node0, node1, and node2 as upPointers to node and then removing node0, node1, and node2 as upPointers, " +
                               "the number of upPointers should be 0 but is " + simpleDomain.getUpPointers().size() + ".");
        }

        return error;
    }

    protected static boolean testAddingAndRemovingDownPointers(Node node,
            boolean error) {
        Node node0 = new Node(0);
        Node node1 = new Node(1);
        Node node2 = new Node(2);
        
        Node downPointer0 = node0;
        Node downPointer1 = node1;
        Node downPointer2 = node2;

        node.addDownPointer(downPointer0);
        SimplifiedNodeDomain simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getDownPointers().size() != 1) {
            System.err.println("When invoking addDownPointer(Node) on a node with no downPointers the number of downPointers should be 1 but is " +
                               simpleDomain.getDownPointers().size() + ".");
        }
        if (!simpleDomain.getDownPointers().contains(0)) {
            System.err.println("When invoking addDownPointer(Node) on a node with no downPointers node 1 should be a downPointer but is not.");
        }

        node.addDownPointer(downPointer1);
        node.addDownPointer(downPointer2);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getDownPointers().size() != 3) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node, the number of downPointers should be 3 but is "    + 
                               simpleDomain.getDownPointers().size() + ".");
        }
        if (!simpleDomain.getDownPointers().contains(0)) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node, node 0 should be a downPointer but is not.");
        }
        if (!simpleDomain.getDownPointers().contains(1)) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node, node 1 should be a downPointer but is not.");
        }
        if (!simpleDomain.getDownPointers().contains(2)) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node, node 2 should be a downPointer but is not.");
        }

        node.removeDownPointer(downPointer0);
        node.removeDownPointer(downPointer2);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getDownPointers().size() != 1) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node and then removing node0 and node2 as downPointers, " + 
                               "the number of downPointers should be 1 but is "    + simpleDomain.getDownPointers().size() + ".");
        }
        if (simpleDomain.getDownPointers().contains(0)) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node and then removing node0 and node2 as downPointers, node 0 should NOT be a downPointer but is.");
        }
        if (simpleDomain.getDownPointers().contains(2)) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node and then removing node0 and node2 as downPointers, node 2 should NOT be a downPointer but is.");
        }
        if (!simpleDomain.getDownPointers().contains(1)) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node and then removing node0 and node2 as downPointers, node 1 should be a downPointer but is NOT.");
        }

        node.removeDownPointer(downPointer1);
        simpleDomain = node.constructSimplifiedNodeDomain();
        if (simpleDomain.getDownPointers().size() != 0) {
            System.err.println("After adding node0, node1, and node2 as downPointers to node and then removing node0, node1, and node2 as downPointers, " +
                               "the number of downPointers should be 0 but is " + simpleDomain.getDownPointers().size() + ".");
        }

        return error;
    }
    
    protected static void printErrorMessage(String type, int size, Node startNode, SimplifiedNodeDomain simplifiedNodeDomain, ExpectedResult expectedResult) {
        errorCount++;

        System.out.println("-------------------------------------------------------------");
        System.out.println(type+" ERROR when HyPeerWebSize = " + size + ", Start Location: " + startNode);
        System.out.println("Node Information:");
        System.out.println(simplifiedNodeDomain);
        System.out.println();
        System.out.println("Expected Information");
        System.out.println(expectedResult);
    }

    protected static void createHyPeerWebWith(int numberOfNodes) {
        hypeerweb.clear();
        Node node0 = new Node(0);
        hypeerweb.addToHyPeerWeb(node0, null);

        for (int i = 1; i < numberOfNodes; i++) {
            Node node = new Node(0);
            node0.addToHyPeerWeb(node);
        }
    }
}
