	/**
	 * @phase 4
	 * @author Everyone!
	 * 
	 * All JUnit 
	 */

package edu.byu.cs340.aabp.hypeerweb.hypeerwebtesting;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import edu.byu.cs340.aabp.hypeerweb.*;
import junit.framework.TestCase;

/*
 * All Testing for HyPeerWeb
 */

public class HyPeerWebTesting extends TestCase 
{

	public void testContents()
	{
		Contents holder = new Contents();
		boolean found = holder.containsKey("NotFound");
		assertEquals(found == false, true);
		found = true;
		holder.set("object", found);
		found = (Boolean)holder.get("object");
		assertEquals(found == true, true);
		holder.get(null);
		holder.set(null, null);
		holder.containsKey(null);
	}
	
	public void testParameters()
	{
		Parameters params = new Parameters(10, "hi");
		params.length();
		params.set(null, null);
		params.keyset();
	}
	
	public void testHeight()
	{
		Height holder1 = new Height();
		Height holder = new Height(2);
		holder.decr();
		holder1.decrBy(-1);
		assertEquals(holder.equals(holder), true);
		holder.equals(-1);
		holder.equals(new Integer(1));
		holder.equals((Object) new Height(11));
		holder.equals((Object) new Integer(11));
		holder.incr();
		holder.incrBy(100);
		holder.setHeight(10);
		holder.toString();
		holder.hashCode();
	}
	
	public void testNode()
	{
		Node node0 = new Node(0,5);
		node0.addDownPointer(new Node(11));
		node0.removeDownPointer(new Node(11));
		node0.addUpPointer(new Node(11));
		node0.removeUpPointer(new Node(11));
		node0.setWebId(null);
		node0.getWebId();
		node0.findHighestNode(node0);
		node0.addNeighbor(new Node(1), true);
		node0.addNeighbor(new Node(15), true);
		insertionDeletionPoint pair = new insertionDeletionPoint();
		pair.setDeletionPoint(node0);
		node0.getchild(pair, new Node(100));
		node0.getContents();
		node0.findToNodeNotRecursive(11);
	}
	
	public void testSimplifiedNodeDomain()
	{
		SimplifiedNodeDomain simple = new SimplifiedNodeDomain(0, 0, 0, 0, 0, 0);
		simple.getWebId();
		simple.getHeight();
		simple.getNeighbors();
		simple.getUpPointers();
		simple.getDownPointers();
		simple.getFold();
		simple.getInverseSurrogateFold();
		simple.getSurrogateFold();
		simple.getState();
		SimplifiedNodeDomain.distanceTo(0, 1);
		simple.setNeighbors(new HashSet<Integer>(5));
		simple.setDownPointers(new HashSet<Integer>(5));
		simple.setUpPointers(new HashSet<Integer>(5));
		simple.toString("Testing...");
		simple.toString();
		simple.containsCloserNode(0, 5);
	}
	
	public void testInsertionDeletionPoint()
	{
		insertionDeletionPoint pair = new insertionDeletionPoint(null, null);
		pair.setDeletionPoint(new Node(1));
		pair.isCapNode();
	}
	
	public void testStutter()
	{
		HyPeerWeb hypeerweb = HyPeerWeb.getSingleton();
		hypeerweb.clear();
		hypeerweb.addNode(new Node(0));
		resize(85);
		resize(2);
		resize(99);
		resize(75);
		resize(86);
		resize(70);
		resize(100);
	}
	
	public void resize(int newSize)
	{
		
		HyPeerWeb hypeerweb = HyPeerWeb.getSingleton();
		int oldSize = hypeerweb.size();
		if (oldSize < 1)
			return;
		
		
		Node startNode = hypeerweb.getNode(oldSize/2);
//		System.out.println("Resizing from "+oldSize+" to "+newSize+" starting from "+startNode.getWebId().getValue()+".");
		
		
		if (newSize > oldSize) // Grow
		{
			for (int size = oldSize+1; size <= newSize; size++)
			{
                hypeerweb.addToHyPeerWeb(new Node(0), startNode);
//                startNode.addToHyPeerWeb(new Node(0));

                assertEquals("Grow Size Check",size,hypeerweb.size());

                for (int i = 0; i < size; i++) 
                {
                    Node nodei = hypeerweb.getNode(i);
                    assertEquals("Grow Check",new ExpectedResult(size, i),nodei.constructSimplifiedNodeDomain());
                }
			}
		}
		
		if (newSize < oldSize) // Shrink
		{
			for (int size = oldSize-1; size >= newSize; size--)
			{
				Node deleteNode = hypeerweb.getNode(size/2);
//				System.out.println("Deleting node("+deleteNode.getWebId().getValue()+") from hypeerweb.size("+hypeerweb.size()+") and size("+size+");");
                hypeerweb.removeNode(deleteNode);
//				System.out.println("Deleted node("+deleteNode.getWebId().getValue()+") from hypeerweb.size("+hypeerweb.size()+") and size("+size+");");

               // assertEquals("Shrink Size Check",size,hypeerweb.size());
		//		size = hypeerweb.size();
                for (int i = 0; i < size; i++) 
                {
                    Node nodei = hypeerweb.getNode(i);
                    assertEquals("Shrink Check",new ExpectedResult(size, i),nodei.constructSimplifiedNodeDomain());
                }
			}
		}
		

	}
	

	/*
	public void testNullNode()
	{
		HyPeerWeb hypeerweb = HyPeerWeb.getSingleton();
		Node node0 = new Node(0);
		hypeerweb.addToHyPeerWeb(node0, null);
        
		
		Node nodeNully = Node.NULL_NODE;
		nodeNully.accept(null, null);
		nodeNully.addDownPointer(null);
		nodeNully.addNeighbor(null);
		//nodeNully.addToHyPeerWeb(null);
		nodeNully.addUpPointer(null);
		nodeNully.constructSimplifiedNodeDomain();
		nodeNully.equals(Node.NULL_NODE);
		nodeNully.getContents();
		nodeNully.getFold();
		nodeNully.getInverseSurrogateFold();
		nodeNully.getInverseSurrogateNeighbors();
		nodeNully.getNeighbors();
		//nodeNully.getState();
		nodeNully.getWebId();
		//nodeNully.goToParent();
		nodeNully.removeDownPointer(null);
		nodeNully.removeNeighbor(null);
		nodeNully.removeUpPointer(null);
		nodeNully.setFold(null);
		nodeNully.setInverseSurrogateFold(null);
		nodeNully.setSurrogateFold(null);
		
		//node0.goToNode(0);
		//nodeNully.getId();
		//nodeNully.getHeight();
		//nodeNully.getLargestNeighbor();
		//nodeNully.getSmallestNWOC();
		//nodeNully.setInverseSurrogateNeighbors(null);
		//nodeNully.setNeighbors(null);
		//nodeNully.goToZero();
		//nodeNully.compareTo(Node.NULL_NODE);
		//nodeNully.setSurrogateNeighbors(null);
		
		nodeNully.setWebId(null);
		
		Node node1 = new Node(1);
        hypeerweb.addToHyPeerWeb(node1, null);
        Node node2 = new Node(2);
        hypeerweb.addToHyPeerWeb(node2, null);
	}
	*/
	public void testSize()
	{
		sizeTestManual(100);
		sizeTestAuto(100);

	}
	
	public void sizeTestManual(int size)
	{
		HyPeerWeb hpw = HyPeerWeb.getSingleton();
		for (int i=0; i<size; i++)
		{
			hpw.clear();
			for (int j=0; j<i; j++)
			{
				hpw.addNode(new Node(0));
			}
			assertEquals("Size",i,hpw.size());
		}
	}
	public void sizeTestAuto(int size)
	{
		HyPeerWeb hpw = HyPeerWeb.getSingleton();
		for (int i=1; i<size; i++)
		{
			createHyPeerWebWith(i, hpw);
			assertEquals("Size",i,hpw.size());
		}
	}
	
	public void testPhase1()
	{
		insertTest(64);
	}
	
	public void insertTest(int HYPEERWEB_SIZE)
	{
		HyPeerWeb hypeerweb = HyPeerWeb.getSingleton();
		
        for (int size = 1; size <= HYPEERWEB_SIZE; size++) 
        {
            hypeerweb.clear();
            Node node0 = new Node(0);
            hypeerweb.addToHyPeerWeb(node0, null);
            Node firstNode = hypeerweb.getNode(0);
            assertEquals("Insertion Check",new ExpectedResult(1, 0),firstNode.constructSimplifiedNodeDomain());

            for (int startNodeId = 0; startNodeId < size - 1; startNodeId++) 
            {
            	createHyPeerWebWith(size-1,hypeerweb);
                
                Node node = new Node(0);
                Node startNode = hypeerweb.getNode(startNodeId);
                hypeerweb.addToHyPeerWeb(node, startNode);
                
                for (int i = 0; i < size; i++) 
                {
                    Node nodei = hypeerweb.getNode(i);
                    assertEquals("Insertion Check",new ExpectedResult(size, i),nodei.constructSimplifiedNodeDomain());
                }
            }
        }
        hypeerweb.addToHyPeerWeb(null, new Node(0));
        hypeerweb.addToHyPeerWeb(Node.NULL_NODE, new Node(0));
        hypeerweb.reload();
        hypeerweb.saveToDatabase();
        hypeerweb.reload("db");
        hypeerweb.contains(Node.NULL_NODE);
        hypeerweb.addNode(null);
        hypeerweb.close();
        HyPeerWeb.getHyPeerWeb();
	}
	
	public void testPhase2()
	{
		deletionTest(64);
	}
	
	public void deletionTest(int HYPEERWEB_SIZE)
	{
		HyPeerWeb hypeerweb = HyPeerWeb.getSingleton();
		
        for (int size = 1; size <= HYPEERWEB_SIZE; size++) 
        {
            for (int deleteNodeId = 0; deleteNodeId < size; deleteNodeId++) 
            {
            	createHyPeerWebWith(size,hypeerweb);
                
                Node deleteNode = hypeerweb.getNode(deleteNodeId);
                deleteNode.removeFromHyPeerWeb();
                
                for (int i = 0; i < size-1; i++) 
                {
                    Node nodei = hypeerweb.getNode(i);
                    assertNotNull("Deletion Null Check node ("+i+") when delete delete node("+deleteNodeId+") from old size("+(size)+")", nodei);
                    assertEquals("Deletion Check node("+deleteNodeId+") from old size("+(size)+")",new ExpectedResult(size-1, i),nodei.constructSimplifiedNodeDomain());
                }
            }
        }
	}
	
	/**
	 * Broadcast: Start from one node in the HyPeerWeb and assert that
	 * every other node receives the message.
	 * 
	 * case 1: Empty HyPeerWeb, 0 Nodes
	 * case 2: 1 Node in the HyPeerWeb
	 * case 3: Balanced medium sized HyPeerWeb (2^n nodes)
	 * case 4: Unbalanced medium HyPeerWeb (num nodes != 2^n)
	 * 
	 * This method runs the BroadCast test on a HyPeerWeb from size 0 through MAX_TEST
	 */
	public void testBroadcastVisitorTest() 
	{
		int MAX_TEST = 100;
		for (int i = 0; i <= MAX_TEST; i++)
		{
			TestBroadcastVisitor(i);
		}
	}
	
	/**
	 * Any node should be able to broadcast to all other nodes. This tests a broadcast
	 * starting from every single node in a HyPeerWeb based on the given size.
	 * 
	 * Ben and Ryan
	 * 
	 * @param size Size of the HyPeerWeb to test
	 */
	public void TestBroadcastVisitor(int size)
	{
		for (int i = 0; i < size; i++)
		{
			TestBroadcastVisitor(size,i);
		}
			
	}

	/**
	 * Tester for the Broadcast behavior. Starting from node broadcastFrom, assert
	 * that every node was visited while broadcasting from this node.
	 * 
	 * Ben and Ryan
	 * 
	 * @param size The size of the HyPeerWeb to test one
	 * @param broadcastFrom The WebId of the node to broadcast from
	 */
	public void TestBroadcastVisitor(int size, int broadcastFrom)
	{
		HyPeerWeb hpw = HyPeerWeb.getSingleton();
		hpw.clear();
		Node[] nodes = new Node[size];
		
		for (int i = 0; i < nodes.length; i++)
		{
			nodes[i] = new Node(0);
			hpw.addNode(nodes[i]);
		}
		
		
		Parameters parameters = BroadcastVisitorTestImpl.createInitialParameters();
		ArrayList<Integer> control = new ArrayList<Integer>();
		ArrayList<Integer> test = new ArrayList<Integer>();

		parameters.set("Test", test);
		
		for (int i = 0; i < nodes.length; i++)
		{
			control.add(i);
		}
		
		assertEquals("Size",0,test.size());

		
		nodes[broadcastFrom].accept(new BroadcastVisitorTestImpl(),parameters);
		
		assertEquals("Size",control.size(),test.size());
		
		Collections.sort(test);
		
		for (int i = 0; i < control.size(); i++)
		{
			assertEquals("Element",control.get(i),test.get(i));

		}
		
		
	}
	
//	private static final int HYPEERWEB_SIZE = 32;
	
	/**
	 * Relational testing
	 * 
	 * This method does a comparison to see if you are searching for self
	 * The method has a comparison to see if the given id is the node's own id.
	 * 
	 * We want to test to see what happens when we test just below, equal, and 
	 * just above the node that we are comparing.
	 * 
	 * 
	 * Author:
	 * Ryan
	 * 
	 */
//	public void testGoToNode()
//	{
//		Node zero = new Node(0);
		
//		assertTrue(zero.goToNode(-1) == null); //invalid node ID
//		assertTrue(zero.goToNode(0) == zero); //go to self
//		assertTrue(zero.goToNode(1) == null); //go to non-existant node
//	}
	
	/**
	 * Tests the hypeerweb in increasing sizes from each node.  Must send a message to every node in hypeerweb
	 * Asserts that the message reached the appropriate target
	 */
	public void testSendMessage()
	{
		int HYPEERWEB_SIZE = 32;
		SendVisitorObject visitor = new SendVisitorObject();
		HyPeerWeb hypeerweb = HyPeerWeb.getSingleton();
		hypeerweb.clear();
	    Node node0 = new Node(0);
		for (int size = 1; size <= HYPEERWEB_SIZE; size++)
        {
	        hypeerweb.addToHyPeerWeb(node0, null);
	        Node firstNode = hypeerweb.getNode(0);
	        
	        Parameters parameters = new Parameters();
	        for (int i = 0; i < size; i++) 
	        {
	            createHyPeerWebWith(size, hypeerweb);
	
	            for(int j = size - 1; j > 0; j--)
	            {
	            	//System.out.println("From " + i + " to " + j );
	            	parameters = visitor.createInitialParameters(j);
	            	int numberOfHops = 0;
	            	boolean reached = false;
	            	parameters.set("HOPS", numberOfHops);
	            	parameters.set("REACHED", reached);
	            	firstNode = hypeerweb.getNode(i);
	            	firstNode.accept(visitor, parameters);
	            	Boolean gotThere = (Boolean)parameters.get("REACHED");
	            	assertEquals(gotThere == true, true);
	            	//System.out.println();
	            	//System.out.println("From " + j + " to " + i);
	            	parameters = visitor.createInitialParameters(i);
	            	firstNode = hypeerweb.getNode(j);
	            	parameters.set("REACHED", reached);
	            	parameters.set("HOPS", numberOfHops);
	            	firstNode.accept(visitor, parameters);
	            	gotThere = (Boolean)parameters.get("REACHED");
	            	assertEquals(gotThere == true, true);
	            	//System.out.println();
	            }
	        }
        }
	}
	
	/**
	 * Helper function to create HypeerWeb of certian sizes
	 * @author Tim
	 * @phase 4
	 * @param numberOfNodes
	 * @param hypeerweb
	 */
	static private void createHyPeerWebWith(int numberOfNodes, HyPeerWeb hypeerweb) 
	{
        hypeerweb.clear();
        Node node0 = new Node(0);
        hypeerweb.addToHyPeerWeb(node0, null);

        for (int i = 1; i < numberOfNodes; i++) 
        {
            Node node = new Node(0);
            node0.addToHyPeerWeb(node);
        }
    }
	
	public class BroadcastVisitorTestImpl extends BroadcastVisitor 
	{

		@Override
		protected void operation(Node node, Parameters parameters) 
		{
//			System.out.println("BroadcastVisitorTest: ID="+node.getWebId().getValue());
			@SuppressWarnings("unchecked")
			ArrayList<Integer> test = (ArrayList<Integer>) parameters.get("Test");
			if (test != null)
			{
				test.add(node.getWebId().getValue());
			}
			else
			{
				System.err.println("Error BroadcastVisitorTest.operation Node("+node.getWebId().getValue()+
						") ArrayList<Integer> test is null");
			}
		}

	}

	
}
