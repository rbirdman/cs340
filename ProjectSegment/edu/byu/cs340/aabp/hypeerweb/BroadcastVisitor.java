package edu.byu.cs340.aabp.hypeerweb;

/**
 * The Broadcast Visitor
 * 
 * @author Benjamin Parry and Ryan Bird
 * @phase 4
 */

public abstract class BroadcastVisitor implements Visitor
{
	protected static final String STARTED_KEY = "STARTED_KEY";
	
	/**
	 * The default constructor.
	 * @designer Benjamin Parry
	 * @phase 4
	 * @pre None
	 * @post True
	 */
	public BroadcastVisitor()
	{
	}
	
	/**
	 * Creates the minimum set of parameters needed when invoking an accept method during a broadcast. 
	 * At the top level (this level) there are no required parameters. If there are more required parameters in a subclass, 
	 * this method is overridden.
	 * @designer Benjamin Parry
	 * @phase 4
	 * @pre None
	 * @post |result| = 0
	 */
	public static Parameters createInitialParameters() 
	{
		return new Parameters();
	}

	/**
	 * The visit operation called by a node in the accept method implementing the broadcast visitor pattern.
	 * @designer Benjamin Parry
	 * @phase 4
	 * @pre node ≠ null AND parameters ≠ null
	 * @post parameters.containsKey(STARTED_KEY) &rArr; <br>
	 * operation(node, parameters).post-condition AND<br>
	 * &forall; neighbor (neighbor &isin; node.neighbors AND neighbor has not and will not be visited by any other node &rArr;<br>
	 * neighbor.accept(this, parameters).post-condition)<br>
	 *  ELSE<br>
	 *  parameters.containsKey(STARTED_KEY) AND<br>
	 *   &exist; node0 (node0 &isin; HyPeerWeb AND node0.webId = 0 AND node0.accept(this, parameters).post-condition)
	 */
	@Override
	public void visit(Node node, Parameters parameters) 
	{
		Node startNode = (Node) parameters.get(STARTED_KEY);
		
		
		if (startNode == null)
		{
			Node zero = node.greedyDown(node);			
			parameters.set(STARTED_KEY, zero);
			operation(zero, parameters);
			for (Node n : zero.getNeighbors())
			{
				n.accept(this, parameters);
			}
		}
		else
		{

			operation(node, parameters);
			int id = node.getWebId().getValue();
			if (id == 0) // Something went wrong...
			{
				System.err.println("Error: VisitorBroadcast.visit() trying to branch with 0.");
				System.err.println(node.constructSimplifiedNodeDomain().toString());
				return;
			}
			int bit = 1;
			//Find the trailing 1 bit
			while ((bit & id) != bit)
			{
				Node neighbor = node.getNeighbor(bit | id);
				if (!neighbor.equals(Node.NULL_NODE))
				{
					neighbor.accept(this, parameters);
				}
				bit = bit * 2;
			}
			
		}
	}
	
	/**
	 * The abstract operation to be performed on all nodes. This operation must be implemented in all concrete subclasses.
	 * @designer Benjamin Parry
	 * @phase 4
	 * @pre node ≠ null AND parameters ≠ null
	 * @post TRUE
	 */
	protected abstract void operation(Node node, Parameters parameters);

}
