package edu.byu.cs340.aabp.hypeerweb;

import java.io.ObjectStreamException;
import java.util.HashSet;

import gui.Main.Command;
import gui.Main.GlobalObjectId;

import edu.byu.cs340.aabp.hypeerweb.phase6.*;

public class NodeProxy
    extends Node
{
    private GlobalObjectId globalObjectId;

    public NodeProxy(GlobalObjectId globalObjectId)
    {
        this.globalObjectId = globalObjectId;
        contents = null;
        webID = null;
        fold = null;
        inverseSurrogateFold = null;
        inverseSurrogateNeighbors = null;
        surrogateNeighbors = null;
        neighbors = null;
        surrogateFold = null;
    }

    public void removeInverseSurrogateNeighbor(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "removeInverseSurrogateNeighbor", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public SimplifiedNodeDomain constructSimplifiedNodeDomain(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "constructSimplifiedNodeDomain", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (SimplifiedNodeDomain)result;
    }

    public Node getSmallestSurrogateNeighbors(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getSmallestSurrogateNeighbors", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public Node largestInverseSurrogateNeighbor(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "largestInverseSurrogateNeighbor", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public void addInverseSurrogateNeighbor(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "addInverseSurrogateNeighbor", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public java.util.HashSet getSurrogateNeighbors(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getSurrogateNeighbors", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (java.util.HashSet)result;
    }

    public java.util.HashSet getInverseSurrogateNeighbors(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getInverseSurrogateNeighbors", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (java.util.HashSet)result;
    }

    public void removeSurrogateNeighbor(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "removeSurrogateNeighbor", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void setInverseSurrogateFold(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "setInverseSurrogateFold", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void setNeighbors(HashSet<Node> p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="java.util.HashSet";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"java.util.HashSet", "setNeighbors", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void setSurrogateNeighbors(HashSet<Node> p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="java.util.HashSet";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"java.util.HashSet", "setSurrogateNeighbors", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
    
    public void setInverseSurrogateNeighbors(HashSet<Node> p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="java.util.HashSet";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"java.util.HashSet", "setInverseSurrogateNeighbors", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
    }
    
    public Node getInverseSurrogateFold(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getInverseSurrogateFold", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public insertionDeletionPoint findInsertionAndDeletion(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "findInsertionAndDeletion", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (insertionDeletionPoint)result;
    }

    public int findParentOfInsertionPoint(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "findParentOfInsertionPoint", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Integer)result;
    }

    public Node smallestNeighborWithNoChild(Node p0, int p1){
        String[] parameterTypeNames = new String[2];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        parameterTypeNames[1] = "int";
        Object[] actualParameters = new Object[2];
        actualParameters[0] = p0;
        actualParameters[1] = p1;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "smallestNeighborWithNoChild", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public Node findToNodeNotRecursive(int p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "int";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "findToNodeNotRecursive", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public void addSurrogateNeighbor(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "addSurrogateNeighbor", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void addDownPointer(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "addDownPointer", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void removeDownPointer(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "removeDownPointer", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void removeUpPointer(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "removeUpPointer", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void removeNeighbor(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "removeNeighbor", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void setSurrogateFold(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "setSurrogateFold", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public Node getSurrogateFold(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getSurrogateFold", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public void addUpPointer(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "addUpPointer", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void addNeighbor(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "addNeighbor", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public WebId getWebId(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "edu.byu.cs340.aabp.hypeerweb.Node", "getWebId", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (WebId)result;
    }

    public java.util.HashSet getNeighbors(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getNeighbors", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (java.util.HashSet)result;
    }

    public Node getNeighbor(WebId p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.WebId";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getNeighbor", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public Node getNeighbor(int p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "int";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getNeighbor", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public void setWebId(WebId p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.WebId";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "setWebId", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void setFold(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "setFold", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public Node getFold(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getFold", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public Node greedyUp(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "greedyUp", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public void setupFolds(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "setupFolds", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public Node greedyDown(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "greedyDown", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public Node getchild(insertionDeletionPoint p0, Node p1){
        String[] parameterTypeNames = new String[2];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.insertionDeletionPoint";
        parameterTypeNames[1] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[2];
        actualParameters[0] = p0;
        actualParameters[1] = p1;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getchild", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    
    public boolean hasChild(Node p0, int p1){
        String[] parameterTypeNames = new String[2];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        parameterTypeNames[1] = "int";
        Object[] actualParameters = new Object[2];
        actualParameters[0] = p0;
        actualParameters[1] = p1;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "hasChild", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Boolean)result;
    }

    public Node findToNode(int p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "int";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "findToNode", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public int findNext(int p0, int p1){
        String[] parameterTypeNames = new String[2];
        parameterTypeNames[0] = "int";
        parameterTypeNames[1] = "int";
        Object[] actualParameters = new Object[2];
        actualParameters[0] = p0;
        actualParameters[1] = p1;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "findNext", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Integer)result;
    }

    public gui.Main.LocalObjectId getLocalID(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getLocalID", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (gui.Main.LocalObjectId)result;
    }

    public void setLocalID(gui.Main.LocalObjectId p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "gui.Main.LocalObjectId";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "setLocalID", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void addToHyPeerWeb(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "addToHyPeerWeb", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public void setupNeighbors(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "setupNeighbors", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public Node findHighestNode(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "findHighestNode", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public insertionDeletionPoint recursiveTerminal(insertionDeletionPoint p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.insertionDeletionPoint";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "recursiveTerminal", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (insertionDeletionPoint)result;
    }

    public insertionDeletionPoint squeezeAlgorithm(insertionDeletionPoint p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.insertionDeletionPoint";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "squeezeAlgorithm", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (insertionDeletionPoint)result;
    }

    public void removeFromHyPeerWeb(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "removeFromHyPeerWeb", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public Node findDeletionPoint(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "findDeletionPoint", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public boolean equals(java.lang.Object p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] = "java.lang.Object";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "equals", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Boolean)result;
    }

    public void replace(Node p0){
        String[] parameterTypeNames = new String[1];
        parameterTypeNames[0] ="edu.byu.cs340.aabp.hypeerweb.Node";
        Object[] actualParameters = new Object[1];
        actualParameters[0] = p0;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "replace", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public Node getParent(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getParent", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Node)result;
    }

    public Object readResolve() throws ObjectStreamException
    {
 //   	System.out.println("Node.readResolve()");
 //   	System.out.println("receiving gid:"+globalObjectId);
    	Node toReturn = this;
    	if(globalObjectId.onSameMachineAs(new GlobalObjectId()))
    	{
//    		System.out.println("I am in readResolve and this is what I got " +((Node)ObjectDB.getSingleton().getValue(getLocalID())).toString());
    		toReturn = (Node)ObjectDB.getSingleton().getValue(getLocalID());
    	}
    	
    	if (globalObjectId.getLocalObjectId().getId() == -1)
    		toReturn = Node.NULL_NODE;
    		
    	
 //   	System.out.println("NodeProxy.readResolve():end toReturn.getWebID().getValue(): "+toReturn.getWebId().getValue());

    	
        return toReturn;
    }

    public void accept(Visitor p0, Parameters p1){
        String[] parameterTypeNames = new String[2];
        parameterTypeNames[0] = "edu.byu.cs340.aabp.hypeerweb.Visitor";
        parameterTypeNames[1] = "edu.byu.cs340.aabp.hypeerweb.Parameters";
        Object[] actualParameters = new Object[2];
        actualParameters[0] = p0;
        actualParameters[1] = p1;
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "accept", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }

    public Object writeReplace() throws ObjectStreamException
    {
        return this; 
    }

    public Contents getContents(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "getContents", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (Contents)result;
    }

    public void disconnect(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(),"edu.byu.cs340.aabp.hypeerweb.Node", "disconnect", parameterTypeNames, actualParameters, false);
        PeerCommunicator.getSingleton().sendASynchronous(globalObjectId, command);
    }
/*
    public int hashCode(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "hashCode", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        try {
        	((Exception)result).printStackTrace();
        } catch (Exception e){}
        return (Integer)result;
        
    }
*/
    public java.lang.String toString(){
        String[] parameterTypeNames = new String[0];
        Object[] actualParameters = new Object[0];
        Command command = new Command(globalObjectId.getLocalObjectId(), "java.lang.Object", "toString", parameterTypeNames, actualParameters, true);
        Object result = PeerCommunicator.getSingleton().sendSynchronous(globalObjectId, command);
        return (java.lang.String)result;
    }

}
