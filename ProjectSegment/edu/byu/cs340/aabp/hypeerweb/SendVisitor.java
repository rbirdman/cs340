package edu.byu.cs340.aabp.hypeerweb;

/**
 * Using the visitor pattern, this is the standard send message.
 * @author Tim Atwood
 * @phase 4
 * 
 * <br>
 *  The Send Visitor
 * <pre>
 * <b>Domain:</b>
 *      None.
 * </pre>
 */
public abstract class SendVisitor implements Visitor 
{
	/**
	 * The default constructor
	 * @designer Tim Atwood
	 * @phase 4
	 * @pre  None
	 * @post  True
	 */
	protected SendVisitor()
	{
	}
	
	/**
	 * Creates the list of parameters needed to send a message
	 * First parameter is the WebID of the Node to recieve the message.
	 * @designer Tim Atwood
	 * @phase 4
	 * @pre  target not equal to null
	 * @param target  The WebID of the Node who will recieve the message.
	 * @post |result| = 1 AND result[0] is the target WebID.
	 */
	public Parameters createInitialParameters(int target)
	{
		Parameters parameters = new Parameters(target);
		assert(parameters.length() == 1);
		return parameters;
	}
	
	
	/**
	 * Abstract operation to perform as a Node passes through.
	 * Can be overwritten to use command pattern on nodes along the path
	 * @designer Tim Atwood
	 * @phase 4
	 * @param node -  The current node in the hypeerweb
	 * @param parameters - The parameters being passed along.
	 * @pre  node not equal to null AND parameters[0] == targetWebID AND parameters[1] == message
	 * @post  TRUE
	 */
	protected abstract void intermediateOperation(Node node, Parameters parameters);
	
	/**
	 * Abstract operation to perform on Node who recieves message
	 * @designer Tim Atwood
	 * @phase 4
	 * @param node -  The current node in the hypeerweb
	 * @param parameters - The parameters being passed along.
	 * @pre  node not equal to null AND parameters[0] == targetWebID AND parameters[1] == message
	 * @post  TRUE
	 */
	protected abstract void targetOperation(Node node, Parameters parameters);
	
	/**
	 * The visit operation called on each node along the path to the intended node in the visitor pattern.
	 * @designer Tim Atwood
	 * @phase 4
	 * @param node -  The current node in the hypeerweb
	 * @param parameters - The parameters being passed along.
	 * @pre  node not equal to null AND parameters[0] == targetWebID AND parameters[1] == message
	 * @post node.webId = parameters.get(TARGET_KEY) => targetOperation(node, parameters).post-condition 
	 *	ELSE
     *   itermediateOperation(node, parameters).post-condition AND
     *   &exist closerNode (
     *           closerNode is in node.neighbors or node.fold AND
     *           distanceBetween(closerNode.webId, parameters.get(TARGET_KEY)) < distanceBetween(node.webId, parameters.get(TARGET_KEY)) AND
     *           there doesn't exist otherNode(
     *                   otherNode is in node.neighbors or node.fold AND
     *                   distance(otherNode.webId, parameters.get(TARGET_KEY) < distance(closerNode.webId, parameters.get(TARGET_KEY)) AND
     *           closerNode.accept(this, parameters).post-condition)
	 */
	
	@Override
	public void visit(Node node, Parameters parameters) 
	{
		if(node != null && parameters != null)
		{
			Node target = (Node)parameters.get("TARGET_KEY");
			
			if(target == null)
			{
				return;
			}
			
			Node closerNode = node.findToNodeNotRecursive(target.getWebId().getValue());
			if(closerNode != null)
			{
				if(node.getWebId().getValue() == target.getWebId().getValue())
				{
					targetOperation(node, parameters);
					Boolean found = (Boolean)parameters.get("REACHED");
					if(found != null)
					{
						found = true;
						parameters.set("REACHED", found);
					}
				}
				else 
				{
					intermediateOperation(node, parameters);
					
					Integer hops = (Integer)parameters.get("HOPS");
					if(hops != null)
					{
						hops++;
						parameters.set("HOPS", hops);
					}
					// Go closer
					closerNode.accept(this, parameters);
				}
			}
			else
			{
				// not in hyperweb
			}
		}
		else
		{
			// Does not meet pre-condition
		}
	}
}
