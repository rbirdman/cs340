package edu.byu.cs340.aabp.hypeerweb;

import gui.Main.GlobalObjectId;
import gui.Main.LocalObjectId;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashSet;

/**
* The standard node in the hypeerweb.  
* @author Tim, Ryan, Reed, Ben
* @phase 1,2,3,4,6
*/

public class Node implements Serializable
{
    protected WebId webID;
    protected HashSet<Node> neighbors;
    protected HashSet<Node> surrogateNeighbors; // downpointers
    protected HashSet<Node> inverseSurrogateNeighbors; // Up pointers
    protected Node fold;
    protected Node surrogateFold;
    protected Node inverseSurrogateFold;
    protected Contents contents;
    public static final Node NULL_NODE = new Node();
    
    public int tempWebId;
    
    private LocalObjectId localID = null;
    
    public Node(int idNumber)
    {
        this();
        
        if(idNumber >= 0)
        {
            webID = new WebId(idNumber);
            fold = this;
        }
    }
    
    
    public Object writeReplace() throws ObjectStreamException
    {
 //   	System.out.println("Node.writeReplace()");
    	GlobalObjectId gid = new GlobalObjectId(localID);
 //   	System.out.println("Sending gid:"+gid);
 //   	System.out.println("Node.writeReplace():end toReturn.getWebID().getValue(): "+getWebId().getValue());
    	return new NodeProxy(gid);
    }
   
    public Object readResolve() throws ObjectStreamException
    {
    	return this;
    }
    
    public Node()
    {
    	contents = new Contents();
        webID = WebId.NULL_WEB_ID;
        neighbors = new HashSet<Node>();
        inverseSurrogateNeighbors = new HashSet<Node>();
        surrogateNeighbors = new HashSet<Node>();
        fold = this; 
        surrogateFold = NULL_NODE;
        inverseSurrogateFold = NULL_NODE;
        localID = new LocalObjectId(-1);
    }
    
    public Node(int idNumber, int height)
    {
        if(idNumber >= 0 && height <= 31 && height >= WebId.locationOfMostSignificantOneBit(idNumber)+1)
        {
            webID = new WebId(idNumber, height);
            neighbors = new HashSet<Node>();
            inverseSurrogateNeighbors= new HashSet<Node>();
            surrogateNeighbors = new HashSet<Node>();
            fold = NULL_NODE;
            surrogateFold = NULL_NODE;
            inverseSurrogateFold = NULL_NODE;
            contents = new Contents();
        }
        else
        {
        }
    }
    
    public void addSurrogateNeighbor(Node surrogateNeighbor) 
    {    
    	surrogateNeighbors.add(surrogateNeighbor);
    }

    public void addInverseSurrogateNeighbor(Node surrogateNeighbor) 
    {    
        inverseSurrogateNeighbors.add(surrogateNeighbor);
    }

    
    public void addDownPointer(Node down)
    {
        surrogateNeighbors.add(down);
    }
    
    
    public void removeDownPointer(Node down)
    {
        surrogateNeighbors.remove(down);
    }
    
    public void addUpPointer(Node up)
    {
        inverseSurrogateNeighbors.add(up);
    }
    
    public void removeUpPointer(Node up)
    {
        inverseSurrogateNeighbors.remove(up);
    }
    
    int NumberOfSetBits(int i)
    {
        i = i - ((i >> 1) & 0x55555555);
        i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
        return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
    }
    
    public void addNeighbor(Node neighbor, boolean validate)
    {
//    	System.out.println("Node.addNeighbor - "+getWebId().getValue()+".addNeighbor("+neighbor.getWebId().getValue()+")");
    	
    	if (validate && NumberOfSetBits(getWebId().getValue() ^ neighbor.getWebId().getValue()) > 1)
    	{
    		System.out.println("Validity Error - '"+getWebId().getValue()+"'.addNeighbor('"+neighbor.getWebId().getValue()+"');");
    	}
        neighbors.add(neighbor);
    }
    
    public HashSet<Node> getNeighbors()
    {
        return neighbors;
    }
    
    public Node getNeighbor(int id)
    {
    	return getNeighbor(new WebId(id));
    }

    
    public Node getNeighbor(WebId id)
    {
    	Node toReturn = NULL_NODE;
    	for(Node n : neighbors)
    	{
    		if (n.getWebId().equals(id))
    		{
    			toReturn = n;
    		}
    	}
    	
    	return toReturn;
    }
    
    public HashSet<Node> getSurrogateNeighbors()
    {
        return surrogateNeighbors;
    }
    
    public HashSet<Node> getInverseSurrogateNeighbors()
    {
        return this.inverseSurrogateNeighbors;
    }
    
    public void removeNeighbor(Node neighbor)
    {
        neighbors.remove(neighbor);
    }
    
    public void removeSurrogateNeighbor(Node neighbor)
    {
        surrogateNeighbors.remove(neighbor);
    }
    
    public void removeInverseSurrogateNeighbor(Node neighbor)
    {
        inverseSurrogateNeighbors.remove(neighbor);
    }
    
    public WebId getWebId()
    {
        if(webID == null)
        {
            return WebId.NULL_WEB_ID;
        }
        return webID;
    }
    
    public void setWebId(WebId newID)
    {
 //   	System.out.println("Modifing webID - '"+getWebId().getValue()+"'.setWebId('"+newID.getValue()+"');");
        webID = newID;
    }
    
    public void setFold(Node newFold)
    {
        fold = newFold;
    }
    
    public Node getFold()
    {
        return fold;
    }
    
    public void setInverseSurrogateFold(Node newInverseSurrogateFold)
    {
        inverseSurrogateFold = newInverseSurrogateFold;
    }
    
    public Node getInverseSurrogateFold()
    {
        return inverseSurrogateFold;
    }
    
    public void setSurrogateFold(Node newSurrogateFold)
    {
        surrogateFold = newSurrogateFold;
    }
    
    public Node getSurrogateFold()
    {
        return surrogateFold;
    }
    
    public boolean equals(Object object) 
    {
        boolean result = (object != null) && (object instanceof Node);
        if (result) 
        {
            Node otherNode = (Node)object;
            result = webID.equals(otherNode.getWebId());
        }
        return result;
    }
    
    public SimplifiedNodeDomain constructSimplifiedNodeDomain()
    {
        HashSet<Integer> simplifiedNeighbors = new HashSet<Integer>();
        HashSet<Integer> simplifiedUpPointers = new HashSet<Integer>();
        HashSet<Integer> simplifiedDownPointers = new HashSet<Integer>();
        
        for(Node holder:neighbors)
        {
            simplifiedNeighbors.add(holder.getWebId().getValue());
        }
        for(Node holder: inverseSurrogateNeighbors)
        {
            simplifiedUpPointers.add(holder.getWebId().getValue());
        }
        for(Node holder: surrogateNeighbors)
        {
            simplifiedDownPointers.add(holder.getWebId().getValue());
        }
        
        SimplifiedNodeDomain toPass = new SimplifiedNodeDomain(webID.getValue(),
                webID.getHeight(),simplifiedNeighbors, simplifiedUpPointers, 
                simplifiedDownPointers, fold.getWebId().getValue(), surrogateFold.getWebId().getValue(), 
                inverseSurrogateFold.getWebId().getValue(), 0);
        
        return toPass;        
    }

    public void addToHyPeerWeb(Node newNode) 
    {
 //   	System.out.println("addToHyPeerWeb() currentSize:"+HyPeerWeb.getSingleton().size());
    	//Our Find Insertion Point

//    	System.out.println("addToHyPeerWeb() starting");

    	Node bottom = greedyDown(this);
    	Node highestNode = greedyUp(bottom); 
    	int parentID = findParentOfInsertionPoint(highestNode);
    	Node insertionPoint = findToNode(parentID);
//    	System.out.println("***********************************************************");

//    	System.out.println("bottom: "+bottom.getWebId().getValue());
//    	System.out.println("highestNode: "+bottom.getWebId().getValue());
//    	System.out.println("parentID: "+bottom.getWebId().getValue());
//    	System.out.println("insertionPoint: "+bottom.getWebId().getValue());

    	/*
    	insertionDeletionPoint pair = new insertionDeletionPoint();
    	pair = findInsertionAndDeletion(greedyUp(this));
    	Node insertionPoint = pair.getInsertionPoint();
    	Node highestNode = pair.getDeletionPoint();
    	*/
    	/*
    	if (!insertionPoint.equals(woodfieldsinsertionPoint))
    	{
    		System.out.println("Woodfields insertion point is different!");
    	}
    	*/
    	insertionPoint.getWebId().incrHeight(1);
    
    	WebId newWebID = new WebId(highestNode.getWebId().getValue() + 1);
//    	System.out.println("newWebID: "+newWebID.getValue());
//    	System.out.println("newNode old: "+newNode.getWebId().getValue());

    	newNode.setWebId(newWebID);
//    	System.out.println("newNode new: "+newNode.getWebId().getValue());

    	insertionPoint.setupNeighbors(newNode);
    	insertionPoint.setupFolds(newNode);
    	
 //   	System.out.println("reference: " + constructSimplifiedNodeDomain());
 //   	System.out.println("reference.class: " + constructSimplifiedNodeDomain());

 //   	System.out.println("newNode: " + newNode.constructSimplifiedNodeDomain());
 //   	System.out.println("newNode.class: " + newNode.constructSimplifiedNodeDomain());
    	

    	int size = newWebID.getValue()+1;

    	if (!newNode.constructSimplifiedNodeDomain().equals(new ExpectedResult(size, newNode.getWebId().getValue())))
    	{
    		System.err.println("ERROR! New node isn't equal.");
    		System.err.println("Expected: " + new ExpectedResult(size, newNode.getWebId().getValue()) );
    		System.err.println("Actual: " + newNode.constructSimplifiedNodeDomain());
    	}
    	
    }
    
    // This sets up folds, surrogate folds, and inverse surrogate folds
    public void setupFolds(Node newNode)
    {
    	if (newNode.getWebId().getValue() == 1)
    	{
    		newNode.setFold(this);
    		this.setFold(newNode);
    	}
    	else if (this.getInverseSurrogateFold().equals(NULL_NODE)) // Even state
    	{
//    		System.out.println("Even State");
    		newNode.setFold(this.getFold());
    		this.setSurrogateFold(this.getFold());
    		this.getFold().setFold(newNode);
    		this.setFold(NULL_NODE);
    		this.getSurrogateFold().setInverseSurrogateFold(this);
    	}
    	else // Odd state
    	{
//    		System.out.println("Odd State");
    		newNode.setFold(this.getInverseSurrogateFold());
    		newNode.getFold().setFold(newNode);
    		newNode.getFold().setSurrogateFold(NULL_NODE);
    		this.setInverseSurrogateFold(NULL_NODE);
    	}
    }
    
    // This sets up neighbors, 
    public void setupNeighbors(Node newNode)
    {
    	//New node doesn't have any inverse surrogate neighbors
    	//Assume that newNode.inverseSurrogateNeighbors is already initialized and empty
    	
    	//New node's surrogate neighbors are the parent's neighbors bigger than the parent.
    	for (Node n : neighbors)
    	{
    		if (n.getWebId().getValue() > this.getWebId().getValue())
    		{
    			newNode.addSurrogateNeighbor(n);
    			n.addInverseSurrogateNeighbor(newNode);
    		}
    	}
    	
    	//New node's neighbors are parent + parent's inverse surrogate neighbors
    	newNode.addNeighbor(this,true);
    	this.addNeighbor(newNode,true);
    	for (Node n : inverseSurrogateNeighbors)
    	{
    		newNode.addNeighbor(n,true);  	
    		n.addNeighbor(newNode,true);
    	}
    	
    	//Remove surrogate and inverseSurrogateNeighbors of now neighbors (clean up)
    	for (Node neighbor : newNode.getNeighbors())
    	{
    		for (Node neighborToCheck : newNode.getNeighbors())
    		{
    			if (neighbor.getSurrogateNeighbors().contains(neighborToCheck))
    			{
    				neighbor.removeSurrogateNeighbor(neighborToCheck);
    				neighborToCheck.removeInverseSurrogateNeighbor(neighbor);
    			}
    		}
    	}
    }    
    
	/**
     * @designer Tim
	 * @phase 2
     * @param currentNode
     * @return Node 0 in HyPeerWeb
     */
    public  Node greedyDown(Node currentNode)
    {
 //   	System.out.println("greedyDown currentNode.class: "+currentNode.getClass());
 //   	System.out.println("greedyDown currentNode.getWebId().getValue(): "+currentNode.getWebId().getValue());
 //   	System.out.println("greedyDown currentNode: "+currentNode.constructSimplifiedNodeDomain());
        if(currentNode.getWebId().getValue() == 0) // Base case.  We found zero.
        {
            return currentNode;
        }
        else // Check all neighbors, surrogateNeighbors, and fold to find smallest value
        {
            Node nextLowestNode = currentNode;
            
            for(Node neighbor : currentNode.getNeighbors())
            {
                if(nextLowestNode.getWebId().getValue() >= neighbor.getWebId().getValue() && neighbor.getWebId().getValue() >= 0)
                {
                    nextLowestNode = neighbor;
                }
            }
            for(Node surrogateNeighbor : currentNode.getSurrogateNeighbors())
            {
                if(nextLowestNode.getWebId().getValue() >= surrogateNeighbor.getWebId().getValue() && surrogateNeighbor.getWebId().getValue() >= 0)
                {
                    nextLowestNode = surrogateNeighbor;
                }
            }
            for(Node inverseSurrogateNeighbor : currentNode.getInverseSurrogateNeighbors())
            {
                if(nextLowestNode.getWebId().getValue() >= inverseSurrogateNeighbor.getWebId().getValue() && inverseSurrogateNeighbor.getWebId().getValue() >= 0)
                {
                    nextLowestNode = inverseSurrogateNeighbor;
                }
            }
            
            if(nextLowestNode.getWebId().getValue() > currentNode.getFold().getWebId().getValue() && currentNode.getFold().getWebId().getValue() >= 0)
            {
                nextLowestNode = currentNode.getFold();
            }
            if(nextLowestNode.getWebId().getValue() > currentNode.getInverseSurrogateFold().getWebId().getValue() && currentNode.getInverseSurrogateFold().getWebId().getValue() >= 0)
            {
                nextLowestNode = currentNode.getInverseSurrogateFold();
            }
            
            if(nextLowestNode.getWebId().getValue() > currentNode.getSurrogateFold().getWebId().getValue() && currentNode.getSurrogateFold().getWebId().getValue() >= 0)
            {
                nextLowestNode = currentNode.getSurrogateFold();
            }
            
            currentNode = greedyDown(nextLowestNode); // Recurse down
        }
        return currentNode;
    }
    
    /**
     * @designer Tim
	 * @phase 2
     * @param currentNode
     * @return Highest Node in the HyPeerWeb
     */
    public Node greedyUp(Node currentNode)
    {
        Node highestNode = currentNode;
    
        for(Node neighbor : currentNode.getNeighbors())
        {
            if(highestNode.getWebId().getValue() <= neighbor.getWebId().getValue())
            {
                highestNode = neighbor;
            }
        }
        for(Node inverseSurrogateNeighbor : currentNode.getInverseSurrogateNeighbors())
        {
            if(highestNode.getWebId().getValue() <= inverseSurrogateNeighbor.getWebId().getValue())
            {
                highestNode = inverseSurrogateNeighbor;
            }
        }
        //for(Node surrogateNeighbor : currentNode.getSurrogateNeighbors())
        //{
          //  if(highestNode.getWebId().getValue() <= surrogateNeighbor.getWebId().getValue())
            //{
              //  highestNode = surrogateNeighbor;
            //}
        //}
        
        if(highestNode.getWebId().getValue() < currentNode.getFold().getWebId().getValue())
        {
            highestNode = currentNode.getFold();
        }
        
        if(highestNode.getWebId().getValue() < currentNode.getInverseSurrogateFold().getWebId().getValue())
        {
            highestNode = currentNode.getInverseSurrogateFold();
        }
        if(highestNode.getWebId().getValue() < currentNode.getSurrogateFold().getWebId().getValue())
        {
            highestNode = currentNode.getSurrogateFold();
        }
        if(!highestNode.equals(currentNode))
        {
            currentNode = greedyUp(highestNode);
        }
        return currentNode;
    }
    
    /**
     * @designer Tim
	 * @phase 2
     */
    public Node findHighestNode(Node bottom)
    {
        Node highestNode = greedyUp(bottom);
        
        return highestNode;
    }
    
    /**
     * @designer Tim
	 * @phase 3
     * @param terminalNode
     * @return Node that is the insertion/deletion point
     */
    public insertionDeletionPoint findInsertionAndDeletionBAD(Node terminalNode)
    {
//    	System.out.print("findInsertionAndDeletion('"+terminalNode.getWebId().getValue()+"')");
    	insertionDeletionPoint pair = new insertionDeletionPoint();
    	pair.setDeletionPoint(terminalNode);
    	// if terminalNode is capnode, go to zero(fold)
    	if(terminalNode.getFold().getWebId().getValue() == 0)
    	{
//    		System.out.println(" ... is cap node");
    		assert(pair.getDeletionPoint().getFold().getWebId().getValue() == 0);
    		pair.setInsertionPoint(pair.getDeletionPoint().getFold());
    		return pair;
    	}
    	else //take its smallest surrogateNeighbor.
    	{
//    		System.out.println(" ... is NOT cap node");
    		pair = recursiveTerminalBAD(pair);
    		return pair;
    	}
    }
    
    public insertionDeletionPoint recursiveTerminalBAD(insertionDeletionPoint pair)
    {

    	if (findParentOfInsertionPoint(pair.getDeletionPoint()) == pair.getInsertionPoint().getWebId().getValue())
    	{
    		return pair;
    	}
    	
    	Node lowestNeighbor = getSmallestSurrogateNeighbors(pair.getDeletionPoint());
    	
    	if (pair.getInsertionPoint().equals(NULL_NODE) || lowestNeighbor.getWebId().getValue() < pair.getInsertionPoint().getWebId().getValue())
    	{
    		pair.setInsertionPoint(lowestNeighbor);
//    		System.out.println("recursiveTerminal - pair.setInsertionPoint("+lowestNeighbor.getWebId().getValue()+");");
    	}
    	
		return squeezeAlgorithmBAD(pair);
    }
    
    /**
     * @designer Tim
     * @param pair
     * @return Hell
     */
    public insertionDeletionPoint squeezeAlgorithmBAD(insertionDeletionPoint pair)
    {
//    	System.out.println("squeezeAlgorithm\tinsertionPoint:"+pair.getInsertionPoint().getWebId().getValue()+"\tdeletionPoint:"+pair.getDeletionPoint().getWebId().getValue());
    	//Sanity check
    	if(pair.getDeletionPoint().getWebId().getValue() <= pair.getInsertionPoint().getWebId().getValue())
    	{
    		System.out.println("HEY! del <= insert");
    		return pair;
    	}
    	
    	if (findParentOfInsertionPoint(pair.getDeletionPoint()) == pair.getInsertionPoint().getWebId().getValue())
    	{
    		return pair;
    	}

    	// what is your largest inverse surrogate neighbor?  if > than deletionPoint, replace and go there.
    	Node largestSurrogateNeighbor=largestInverseSurrogateNeighbor(pair.getInsertionPoint());

    	if(largestSurrogateNeighbor.getWebId().getValue() > pair.getDeletionPoint().getWebId().getValue())
    	{
//    		System.out.println("squeezeAlgorithm - pair.setDeletionPoint("+largestSurrogateNeighbor.getWebId().getValue()+");");
    		pair.setDeletionPoint(largestSurrogateNeighbor);
    		return recursiveTerminalBAD(pair);
    	}
    	else
    	{
    		Node smallest = smallestNeighborWithNoChild(pair.getInsertionPoint(),pair.getDeletionPoint().getWebId().getHeight());
    		
    		

    		pair.setInsertionPoint(smallest);
    		pair = recursiveTerminalBAD(pair);
    		return pair;
    		// else go to smallest neighbor without child.
    	}
    }
    /**
     * @designer Reed
     */
    public Node getSmallestSurrogateNeighbors(Node deletionPoint)
    {
    	Node lowestNeighbor = null;
    	for(Node neighbor : deletionPoint.getSurrogateNeighbors())
		{
			if(lowestNeighbor == null || neighbor.getWebId().getValue() < lowestNeighbor.getWebId().getValue())
			{
				lowestNeighbor = neighbor;
			}
		}
    	return lowestNeighbor;
    }
    /**
     * @designer Reed
     */
    public Node getchild(insertionDeletionPoint pair,Node smallestNeighbor)
    {
    	for(Node neighbor : pair.getDeletionPoint().getNeighbors())
    	{
    		if(smallestNeighbor.getWebId().getValue() > neighbor.getWebId().getValue())
    		{
    			smallestNeighbor = neighbor;
    		}
    	}
    	return smallestNeighbor;
    }
    /**
     * @designer Reed
     */
    public Node largestInverseSurrogateNeighbor(Node insertionPoint)
    {
    	Node largestSurrogateNeighbor = null;
		for(Node neighbor : insertionPoint.getInverseSurrogateNeighbors()) 
		{
			if(largestSurrogateNeighbor == null || largestSurrogateNeighbor.getWebId().getValue() < neighbor.getWebId().getValue())
			{
				largestSurrogateNeighbor = neighbor;
			}
		}
    	return largestSurrogateNeighbor;
    }
    /**
     * @designer Reed
     */
    public Node smallestNeighborWithNoChild(Node insertionPoint, int height)
    {
    	Node smallest = null;
		for(Node neighbor : insertionPoint.getNeighbors()) 
		{
			if(smallest == null || smallest.getWebId().getValue() > neighbor.getWebId().getValue())
			{
				if(!hasChild(neighbor,height))
				{
					smallest = neighbor;
				}
				
			}
		}
    	return smallest;
    }
    

    
    /**
     * 
     * 
     * @return NULL NODE if no parent is found.  Otherwise, returns the parent
     */
    public Node getParent()
    {
    	String binaryRepresentationOfNewNode = Integer.toBinaryString(this.getWebId().getValue());
    	String firstBit = binaryRepresentationOfNewNode.substring(0,1);
    	firstBit = "0";
        
        String parentBinRep = firstBit + binaryRepresentationOfNewNode.substring(1);
        int parentId = Integer.valueOf(parentBinRep, 2);
        Node parent = Node.NULL_NODE;
        for(Node neighbor : this.getNeighbors())
        {
        	if(neighbor.getWebId().getValue() == parentId)
        	{
    		  parent = neighbor;
        	}
        }
    	return parent;
    }
    
    public boolean hasChild(Node currentNode, int height)
    {
    	
    	boolean toReturn = false;
    	        
    	int parentId = currentNode.getWebId().getValue() | (1 << (height-1));
    	
    	for(Node neighborToCheck : currentNode.getNeighbors()) // child of node
		{			
			if(neighborToCheck.getWebId().getValue() == parentId)
			{
				toReturn = true;
				break;
			}
		}
    	
 //   	System.out.println("hasChild parentID:"+parentId+"\tresult:"+toReturn);
 //   	System.out.println(currentNode.constructSimplifiedNodeDomain().toString());
    	return toReturn;
    }
    
    public int findParentOfInsertionPoint(Node highestNode)
    {
    	int newNodeWebID = highestNode.getWebId().getValue() + 1; // This is the insertion point
        String binaryRepresentationOfNewNode = Integer.toBinaryString(newNodeWebID);
        String firstBit = binaryRepresentationOfNewNode.substring(0,1);
        
        if(firstBit.contains("1"))
        {
            firstBit = "0";
        }
        else
        {
            firstBit = "1";
        }
        
        String parentBinRep = firstBit + binaryRepresentationOfNewNode.substring(1);
        int parentId = Integer.valueOf(parentBinRep, 2);
        
        return parentId;
    }
    
    public Node findToNode(int toNode)
    {
    	if (getWebId().getValue() == toNode)
    	{
    		return this;
    	}
    	else
    	{
    		Node nextNode = null;
    		int nextId = findNext(getWebId().getValue(),toNode);
    		for (Node n : getNeighbors())
    		{
    			if (n.getWebId().getValue() == nextId)
    			{
    				nextNode = n;
    			}
    		}
    		
    		if (nextNode == null)
    		{
    			System.out.println("Error in findToNode(). nextNode == null");
    			System.out.println("WebID:"+getWebId().getValue()+"\ttoNode:"+toNode+"\tnextId:"+nextId);
    			System.out.println(constructSimplifiedNodeDomain().toString());
    			return null;
    		}
    		
    		return nextNode.findToNode(toNode);
    	}
    }
	
	public int findNext(int current, int toGetTo)
	{
		
		if (current == toGetTo)
			return current;
		
		int xor = current ^ toGetTo;
		int bit = 1;
		// Look for the first different bit that can change to 0
		while ((bit != Integer.MIN_VALUE) && (((bit & xor) != bit) || ((bit & current) == 0)))
		{
			bit = bit * 2;
		}
		
		// If it couldn't find one, find the first different bit to change to 1
		if (bit == Integer.MIN_VALUE)
		{
			bit = 1;
			while ((bit & xor) != bit)
				bit = bit * 2;
		}
			
			
		int toReturn = current ^ bit;
		
		return toReturn;
	}
	
	//Phase 3
	/**
	 * @phase3
	 * @designer Ben & Ryan
	 * 
	 * Removes this node from the HyPeerWeb by removing any connections to it and rebalancing the HyPeerWeb as needed.
	 */
	public void removeFromHyPeerWeb() 
	{
//		System.out.println("removeFromHyPeerWeb() - "+getWebId().getValue());
		Node deletionPoint = findDeletionPoint();
		
		int oldDeletionID = deletionPoint.getWebId().getValue();
		
		if (deletionPoint.getWebId().getValue() == 0)
		{
			HyPeerWeb.getSingleton().clear();
		} 
		
//		System.out.println("removeFromHyPeerWeb pre deletionPoint ID:"+deletionPoint.getWebId().getValue());
		if (equals(deletionPoint))
		{
			HyPeerWeb.getSingleton().setLastNode(greedyDown(this));
		}
		else
		{
			HyPeerWeb.getSingleton().setLastNode(deletionPoint);
		}
		
		deletionPoint.disconnect();

		replace(deletionPoint);
		
		setWebId(new WebId(-1));
		
		if (oldDeletionID == 1)
		{
			deletionPoint.setFold(deletionPoint);
		}
		
//		System.out.println("removeFromHyPeerWebpost post deletionPoint ID:"+deletionPoint.getWebId().getValue());
	}
	
	public void setNeighbors(HashSet<Node> newNeighbors) {
		neighbors = newNeighbors;
	}
	
	public void setSurrogateNeighbors(HashSet<Node> newNeighbors) {
		surrogateNeighbors = newNeighbors;
	}
	
	public void setInverseSurrogateNeighbors(HashSet<Node> newNeighbors) {
		inverseSurrogateNeighbors = newNeighbors;
	}
	
	/**
	 * @phase3
	 * @designer Ben & Ryan
	 * 
	 * @param replacement Takes the place of this node. Replacement replaces me.
	 */
	public void replace(Node replacement) // Replace this with replacement
	{
		if (equals(replacement))
		{
			return;
		}
		//Set ID
		replacement.setWebId(getWebId());
		
		//Folds, Surrogate Folds, Inverse Surrogate Folds
		replacement.setFold(getFold());
		replacement.setSurrogateFold(getSurrogateFold());
		replacement.setInverseSurrogateFold(getInverseSurrogateFold());
		
		if (!getFold().equals(NULL_NODE))
		{
			getFold().setFold(replacement);
		}
		
		if (!getSurrogateFold().equals(NULL_NODE))
		{
			getSurrogateFold().setInverseSurrogateFold(replacement);
		}
		
		if (!getInverseSurrogateFold().equals(NULL_NODE))
		{
			getInverseSurrogateFold().setSurrogateFold(replacement);
		}
		
		//Neighbors, Surrogate Neighbors, Inverse Surrogate Neighbors
		replacement.setNeighbors(neighbors);
		for (Node n : neighbors)
		{
			n.addNeighbor(replacement,true);
			n.removeNeighbor(this);
		}
		
		replacement.setSurrogateNeighbors(surrogateNeighbors);
		for (Node n : surrogateNeighbors)
		{
			n.removeInverseSurrogateNeighbor(this);
			n.addInverseSurrogateNeighbor(replacement);
		}
		
		/*
		replacement.getInverseSurrogateNeighbors().clear();
		for (Node n : inverseSurrogateNeighbors)
		{
			replacement.addInverseSurrogateNeighbor(n);
			n.removeSurrogateNeighbor(this);
			n.addSurrogateNeighbor(replacement);
		}
		*/
		
		replacement.setSurrogateNeighbors(inverseSurrogateNeighbors);
		for (Node n : inverseSurrogateNeighbors)
		{
			n.removeSurrogateNeighbor(this);
			n.addSurrogateNeighbor(replacement);
		}		
		
	}
	
	/**
	 * @phase3
	 * @designer Ben & Ryan
	 * 
	 * Removes all connections to this node.
	 * 
	 * @postcondition This node is hanging by a thread.
	 */
	public void disconnect()
	{
		DisconnectTemplate disc = null;
		if (getWebId().getValue() == 1)
		{
			disc = new DisconnectSecondToLastNode();
		}
		else if (getFold().getInverseSurrogateFold().equals(NULL_NODE)) // Even state
    	{
			disc = new DisconnectEvenNode();
    	}
    	else // Odd state
    	{
    		disc = new DisconnectOddNode();
    	}
		
		disc.disconnect(this);
		
		/*
//		System.out.println("disconnect - "+getWebId().getValue());
		// Height
		getParent().getWebId().incrHeight(-1);
		
		//Folds, Surrogate Folds, Inverse Surrogate Folds
		if (getWebId().getValue() == 1)
		{
			lastState.do();
			getFold().setFold(getFold());
		}
		else if (getFold().getInverseSurrogateFold().equals(NULL_NODE)) // Even state
    	{
			evenState.do();
    		getFold().setFold(NULL_NODE);
    		getFold().setSurrogateFold(getParent());
    		getParent().setInverseSurrogateFold(getFold());
    		
    	}
    	else // Odd state
    	{
    		oddState.do();
    		getFold().setFold(getFold().getInverseSurrogateFold());
    		getFold().getFold().setFold(getFold());
    		getFold().getFold().setSurrogateFold(NULL_NODE);
    		getFold().setInverseSurrogateFold(NULL_NODE);
    	}
		
		Node parent = getParent();
		//Neighbors, Surrogate Neighbors, Inverse Surrogate Neighbors
		for (Node n : neighbors)
		{
			n.removeNeighbor(this);
			if (!n.equals(parent))
			{
				n.addSurrogateNeighbor(parent);
				parent.addInverseSurrogateNeighbor(n);
			}
		}
		
		for (Node n : surrogateNeighbors)
		{
			n.removeInverseSurrogateNeighbor(this);
		}
		*/
	}
	
	/**
	 * @phase3
	 * @designer Ben & Ryan
	 * 
	 * Finds the deletion point (highest node in the HyPeerWeb).
	 * 
	 * @return Node that is the deletion point
	 */
	public Node findDeletionPoint()
	{
		Node bottom = greedyDown(this);
		Node top = greedyUp(bottom);
		return top;
		/*
    	insertionDeletionPoint pair = new insertionDeletionPoint();
    	pair = findInsertionAndDeletion(greedyUp(this));
    	return pair.getDeletionPoint();
		 */
 	}
	
	/**
	 * The standard Node's accept function to use visitor pattern
	 * @designer Tim
	 * @phase 4
	 * @param visitor
	 * @param parameters
	 */
	public void accept(Visitor visitor, Parameters parameters)
	{
		if(visitor != null && parameters != null) 
		{
			visitor.visit(this, parameters);
		}
	}

	public Contents getContents()
	{
		return contents;
	}
	
	/**
	 * I need a non recursive function for send Visitor
	 * @designer Tim
	 * @phase 4
	 * @param toNode
	 * @return Node that has the same id value as parameter.
	 */
	public Node findToNodeNotRecursive(int toNode)
    {
    	if (getWebId().getValue() == toNode)
    	{
    		return this;
    	}
    	else
    	{
    		Node nextNode = null;
    		int nextId = findNext(getWebId().getValue(),toNode);
    		for (Node n : getNeighbors())
    		{
    			if (n.getWebId().getValue() == nextId)
    			{
    				nextNode = n;
    			}
    		}
    		
    		if (nextNode == null)
    		{
    			System.out.println("Error in findToNode(). nextNode == null");
    			System.out.println("WebID:"+getWebId().getValue()+"\ttoNode:"+toNode+"\tnextId:"+nextId);
    			System.out.println(constructSimplifiedNodeDomain().toString());
    			int i = 0 / 0;
    		}
    		
    		return nextNode;
    	}
    }

	public LocalObjectId getLocalID()
	{
		return localID;
	}

	public void setLocalID(LocalObjectId localID) 
	{
		this.localID = localID;
	}
}